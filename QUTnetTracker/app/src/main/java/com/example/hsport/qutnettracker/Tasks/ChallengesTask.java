package com.example.hsport.qutnettracker.Tasks;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;

import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.example.hsport.qutnettracker.MainActivity;
import com.example.hsport.qutnettracker.NotificationActivity;
import com.example.hsport.qutnettracker.R;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by hkthilina on 3/13/2016.
 */
public class ChallengesTask extends AsyncTask<String, Void, List<String>> {

    MainActivity mainActivity = new MainActivity();
    private Context mContext;
    //  private int NOTIFICATION_ID = 1;
    private Notification mNotification;
    private NotificationManager mNotificationManager;
    private String username;
    List<HashMap<String, String>> aList;
    public static int idd = 0;

    public static String Challenger;
    public static String Block;
    public static String Message;

    public ChallengesTask(Context context) {

        mContext = context;
        // Name=name;
    }

    private List<String> Names = new ArrayList<String>();

    @Override
    protected List<String> doInBackground(String... params) {
        try {

            Date dte = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
            username = (String) params[0];
            String cdate = sdf.format(dte);

            String link = "http://sharethat.us/QUT/SearchChallenges.php";
            String data = URLEncoder.encode("username", "UTF-8") + "=" + URLEncoder.encode(username, "UTF-8");
            data += "&" + URLEncoder.encode("cdate", "UTF-8") + "=" + URLEncoder.encode(cdate, "UTF-8");


            URL url = new URL(link);

            URLConnection conn = url.openConnection();

            conn.setDoOutput(true);
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());

            wr.write(data);
            wr.flush();

            BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));

            StringBuilder sb = new StringBuilder();
            String line = null;


            //   Log.d("ChallengesTask debug", "ChallengesTask 1");
            // int i=0;
            // Read Server Response
            while ((line = reader.readLine()) != null) {
                //    Log.d("ChallengesTask names1", line);
                sb.append(line);

            }
            String String_Response = sb.toString(); // this is your web response

            if (String_Response.equals("")) {
                Names = null;
            } else {

                Names = Arrays.asList(String_Response.split("\\s*,\\s*"));
            }
            //  Log.d("ChallengesTask", "Responce is " + String_Response);


            return Names;
        } catch (Exception e) {
            Log.e("ChallengesTask ", "2");
///          Log.d("Friends List Exception", e.getMessage().toString());
            return Names;
        }
    }

    @Override
    protected void onPostExecute(List<String> ss) {
        super.onPostExecute(ss);

        aList = new ArrayList<HashMap<String, String>>();
        int id = 0;

        try {

            String s=ss.get(0);
            String[] parts = s.split(":"); // escape .
            String Challenger = parts[0];
            String Block = parts[1];


        for (int i = 0; i < ss.size(); i++)
        {
            Log.d("List I got","List I got:"+ss.get(i));

            //object = ss.get(i);
            //do something with i
        }

        //if(ss!=null)

            //  Log.d("Iterartion ", "Iterartion-y");
            if (!ss.isEmpty() || ss != null) {
                sendNotification(Challenger+" challenged you"," has challenged you", mContext, id,Block);

                //    Log.d("Iterartion ", "Iterartion-x");
                id++;

            }
        } catch (Exception e) {
        }

    }

    private void sendNotification(String contentTitle, String contentText, Context context, int NOTIFICATION_ID,String Block) {
        // Create an explicit content Intent that starts the main Activity.

        // PendingIntent dismissIntent = NotificationActivity.getDismissIntent(notificationId, context);

        Intent notificationIntent = new Intent(context, MainActivity.class);
        PendingIntent dismissIntent = NotificationActivity.getDismissIntent(NOTIFICATION_ID, context);
        notificationIntent.putExtra("mapFragment", "favoritesMenuItem");

        mainActivity.setFirstTime();


        // Construct a task stack.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);

        // Add the main Activity to the task stack as the parent.
        stackBuilder.addParentStack(MainActivity.class);

        // Push the content Intent onto the stack.
        stackBuilder.addNextIntent(notificationIntent);

        // Get a PendingIntent containing the entire back stack.
        PendingIntent notificationPendingIntent =
                stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        // Get a notification builder that's compatible with platform versions >= 4
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);

   //    Bitmap bigBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.challenging);
        // Define the notification settings.
        builder.setSmallIcon(R.mipmap.ic_launcher)
                // In a real app, you may want to use a library like Volley
                // to decode the Bitmap.
                // .setSmallIcon(android.R.drawable.btn_star)
                .setColor(Color.BLUE)
                .setAutoCancel(true)
                .setContentTitle(contentTitle)
                .setContentText(contentText)

                .setContentIntent(notificationPendingIntent);



        builder.setVibrate(new long[]{1000, 1000, 1000, 1000, 1000});
             //   .addAction(R.drawable.sad, "Dismiss", dismissIntent);



        Bitmap bigBitmap;
        switch(Block) {
            case "A Block":
                bigBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.a);
                break;
            case "B Block":
                bigBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.b);
                break;
            case "C Block":
                bigBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.c);
                break;
            case "D Block":
                bigBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.d);
                break;
            case "E Block":
                bigBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.e);
                break;
            case "F Block":
                bigBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.f);
                break;
            case "G Block":
                bigBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.g);
                break;
            case "H Block":
                bigBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.h);
                break;
            case "M Block":
                bigBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.m);
                break;
            case "N Block":
                bigBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.n);
                break;
            case "O Block":
                bigBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.o);
                break;
            case "P Block":
                bigBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.p);
                break;
            case "Q Block":
                bigBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.q);
                break;
            case "R Block":
                bigBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.r);
                break;
            case "S Block":
                bigBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.s);
                break;
            case "V Block":
                bigBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.v);
                break;
            case "X Block":
                bigBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.x);
                break;
            case "Y Block":
                bigBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.y);
                break;
            case "Z Block":
                bigBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.z);
                break;
            default:
                bigBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.x);
        }


//
//            Bitmap bigBitmap = BitmapFactory.decodeResource(this.getResources(), R.drawable.a);
        builder.setStyle(new android.support.v7.app.NotificationCompat.BigPictureStyle()
                .bigPicture(bigBitmap));



        // Dismiss notification once the user touches it.
        // builder.setAutoCancel(true);

        // Get an instance of the Notification manager
        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        // Issue the notification


        mNotificationManager.notify(NOTIFICATION_ID, builder.build());

    }
}


