package com.example.hsport.qutnettracker.Tasks;

import android.content.ContentValues;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.example.hsport.qutnettracker.GroupFragment;
import com.example.hsport.qutnettracker.JSONParser;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hkthilina on 3/19/2016.
 */
public class addGroupTask extends  AsyncTask<String,Void,String> {

    public JSONObject json ;
    private static final String TAG_SUCCESS = "code";
    private GroupFragment context1;
    private String groupname;
    private String groupType;


    public addGroupTask(GroupFragment context)
    {

        context1=context;
        // Name=name;
    }

    @Override
    protected String doInBackground(String... params) {
        try{
            ContentValues contentValues = new ContentValues();
            groupname = (String) params[0];
            groupType = (String) params[1];


            List<NameValuePair> pair = new ArrayList<>();
            pair.add(new BasicNameValuePair("Gr_Name", groupname));
            pair.add(new BasicNameValuePair("Status", groupType));



            json = JSONParser.makeHttpRequest("http://sharethat.us/QUT/addGroup.php", "GET", pair);

          //  Log.d("Create Response", json.toString());

            return  "ok";

        }catch(Exception e){
            //Add an exception saying the name already exist
            return new String("Exception: " + e.getMessage());

        }
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);


      Log.d("Error","s");
    }
}
