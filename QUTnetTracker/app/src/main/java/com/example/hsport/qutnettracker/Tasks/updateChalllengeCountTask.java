package com.example.hsport.qutnettracker.Tasks;

import android.content.ContentValues;
import android.os.AsyncTask;
import android.util.Log;

import com.example.hsport.qutnettracker.JSONParser;
import com.example.hsport.qutnettracker.MainActivity;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by hkthilina on 4/6/2016.
 */
public class updateChalllengeCountTask  extends AsyncTask<String,Void,String> {

    public JSONObject json ;
    private static final String TAG_SUCCESS = "code";
    private MainActivity context1;
    private String userID;
    private String Today;
    private String extraSteps;
    private String Totsteps;
    private static int ID;


    @Override
    protected String doInBackground(String... params) {


        try{
            //ContentValues contentValues = new ContentValues();
            userID = (String) params[0];

            Today = (String) params[1];





            String dates="";

          //  Log.d("Today", "date is" + Today);
            //Log.d("Today", "userID is" + userID);

         //   Log.d("Async", "AsyncextraSteps3" + extraSteps);


            List<NameValuePair> pair = new ArrayList<>();
            pair.add(new BasicNameValuePair("Member", userID));
            pair.add(new BasicNameValuePair("C_Date", Today));




            json = JSONParser.makeHttpRequest("http://sharethat.us/QUT/updateChallengeCount.php", "GET", pair);

          //  Log.d("Create Response","Create Response ha ha"+json.toString());

            return  "ok";

        }catch(Exception e){
            //Add an exception saying the name already exist
            return new String("Exception: " + e.getMessage());

        }
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);

        //Log.d("Result is","Result is:" +s);
    }
}
