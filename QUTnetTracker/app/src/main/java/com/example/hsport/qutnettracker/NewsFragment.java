package com.example.hsport.qutnettracker;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.hsport.qutnettracker.Tasks.ChallengeCountTask;
import com.example.hsport.qutnettracker.Tasks.CompletedchallengesFeedtask;
import com.example.hsport.qutnettracker.Tasks.NewsTask;
import com.example.hsport.qutnettracker.Tasks.challengesFeedtask;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import me.leolin.shortcutbadger.ShortcutBadger;


/**
 * A simple {@link Fragment} subclass.
 */
public class NewsFragment extends Fragment {
    public MainActivity mainActivity = new MainActivity();
    public Spinner spinner;
    public static NewsTask newsTask;
    public static ChallengeCountTask challengeCountTask;
    public static challengesFeedtask newschallengesTask;
    public static CompletedchallengesFeedtask completedchallengesFeedtask;
    ListView lv;
    public static List<String> list = new ArrayList<String>();
    static List<String> Names = new ArrayList<String>();
    public static View rootView;
    private TextView txtView;

    public static List<HashMap<String, String>> aList;

    public NewsFragment() {
        // Required empty public constructor
        aList = new ArrayList<HashMap<String, String>>();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_gallery, container, false);

        txtView = (TextView) rootView.findViewById(R.id.txt1Messeage);
        int[] flags = new int[]{
                R.drawable.man1,
                R.drawable.sad,
        };
        try {
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DATE, -0);
            String today = dateFormat.format(cal.getTime());

            Calendar cal1 = Calendar.getInstance();
            cal1.add(Calendar.DATE, -1);
            String yesterday = dateFormat.format(cal1.getTime());


            int id = mainActivity.getUserID();
            String name = mainActivity.getUserName();

            Log.d("setChallengeCountTask", "setChallengeCountTask:" + name);

            challengeCountTask = (ChallengeCountTask) new ChallengeCountTask(this).execute(name, today);
            completedchallengesFeedtask = (CompletedchallengesFeedtask) new CompletedchallengesFeedtask(this).execute(Integer.toString(id), today);
         //   completedchallengesFeedtask = (CompletedchallengesFeedtask) new CompletedchallengesFeedtask(this).execute(Integer.toString(id), yesterday);

            newsTask = (NewsTask) new NewsTask(this).execute(Integer.toString(id), today);
           // newsTask = (NewsTask) new NewsTask(this).execute(Integer.toString(id), yesterday);
            newschallengesTask = (challengesFeedtask) new challengesFeedtask(this).execute(Integer.toString(id), today);
        //    newschallengesTask = (challengesFeedtask) new challengesFeedtask(this).execute(Integer.toString(id), yesterday);


        } catch (Exception e) {
            Log.e("Exception1:", e.getMessage());
        }
        setRetainInstance(true);
        return rootView;
    }

    public void setChallengeCountTask(int count) {
        try {
            //  count=0;
            Log.d("setChallengeCountTask", "setChallengeCountTask:" + count);
            // globally
            TextView myAwesomeTextView = (TextView) rootView.findViewById(R.id.txtChallengesCount);

//in your OnCreate() method
            if (count > 0) {

                if (count == 1)
                    myAwesomeTextView.setText("You have " + count + " challenge from your friends");
                else
                    myAwesomeTextView.setText("You have " + count + " challenges from your friends");


                myAwesomeTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        ChallengesForMeFragment challengesForMeFragment = new ChallengesForMeFragment();
                        FragmentManager fragmentManager = getFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        //  FragmentManager fragmentManager = getFragmentManager();
                        // android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                        fragmentTransaction.replace(R.id.fragment_container, challengesForMeFragment);
                        fragmentTransaction.addToBackStack(null);
                        fragmentTransaction.commit();

                        //mainActivity.getSupportActionBar().setTitle("Offered Challenges");


                    }
                });

            } else myAwesomeTextView.setVisibility(View.GONE);

        } catch (Exception e) {

        }
    }


    public void setList(List<String> list) {
        this.list = list;
        int[] flags = new int[]{
                R.drawable.walking,
                R.drawable.sadface,
        };
        try {

            for (String r : list) {
                Log.e("String", "Received String " + r);
                String[] parts = r.split("::");
                HashMap<String, String> hm = new HashMap<String, String>();
                hm.put("txt", parts[0] + " has walked a total of " + parts[1] + " steps");
                hm.put("total", "He has walked " + parts[2] + " extra steps on " + parts[3]);
                hm.put("extra", Integer.toString(flags[0]));
                hm.put("time", parts[4]);
                aList.add(hm);
                txtView.setVisibility(View.GONE);

            }
            // Keys used in Hashmap

        } catch (Exception e) {
            Log.e("Exception2:", e.getMessage());
            List<HashMap<String, String>> aList = new ArrayList<HashMap<String, String>>();
            HashMap<String, String> hm = new HashMap<String, String>();
            hm.put("txt", "No news yet");
            //  hm.put("total", "Total steps : " + parts[1] + " Extra Steps: " + parts[2]);
            hm.put("total", "Please check in a while");
            hm.put("extra", Integer.toString(flags[1]));
            aList.add(hm);
            // Keys used in Hashmap

        }
    }

    public void setchallengesList(List<String> list) {
        this.list = list;
        int[] flags = new int[]{
                R.drawable.trophy,
                R.drawable.sadface,
        };
        try {

            for (String r : list) {
                Log.e("String", "Received String " + r);
                String[] parts = r.split("::");
                HashMap<String, String> hm = new HashMap<String, String>();
                hm.put("txt", parts[0] + " has completed a total of " + parts[1] + " challenges");
                hm.put("total", "on" +
                        " " + parts[2]);
                hm.put("extra", Integer.toString(flags[0]));
                hm.put("time", parts[3]);
                aList.add(hm);

                txtView.setVisibility(View.GONE);
            }

            createNewsFeed();
            // Keys used in Hashmap
//            String[] from = {"extra", "txt", "total"};
//            // Ids of views in listview_layout
//            int[] to = {R.id.flag, R.id.txt, R.id.cur};
//
//            SimpleAdapter adapter = new SimpleAdapter(getActivity(), aList, R.layout.listview_layout, from, to);
//
//            ListView listView = (ListView) rootView.findViewById(R.id.challenegeList);
//
//            listView.setAdapter(adapter);
        } catch (Exception e) {
            Log.e("Exception2:", e.getMessage());
            List<HashMap<String, String>> aList = new ArrayList<HashMap<String, String>>();
            HashMap<String, String> hm = new HashMap<String, String>();
            hm.put("txt", "No news yet");
            //  hm.put("total", "Total steps : " + parts[1] + " Extra Steps: " + parts[2]);
            hm.put("total", "Please check in a while");
            hm.put("extra", Integer.toString(flags[1]));
            aList.add(hm);
            createNewsFeed();
            // Keys used in Hashmap
//            String[] from = {"extra", "txt", "total"};
//            // Ids of views in listview_layout
//            int[] to = {R.id.flag, R.id.txt, R.id.cur};
//
//            SimpleAdapter adapter = new SimpleAdapter(getActivity(), aList, R.layout.listview_layout, from, to);
//
//            ListView listView = (ListView) rootView.findViewById(R.id.challenegeList);
//
//            listView.setAdapter(adapter);
        }
    }

    public void setChallengesCompletedList(List<String> list) {
        this.list = list;
        int[] flags = new int[]{
                R.drawable.completed,
                R.drawable.sadface,
        };
        try {
            // List<HashMap<String, String>> aList = new ArrayList<HashMap<String, String>>();
            for (String r : list) {
                Log.e("String", "Received String " + r);
                String[] parts = r.split("::");
                HashMap<String, String> hm = new HashMap<String, String>();
                hm.put("txt", parts[0] + " has completed a challenge by walking to " + parts[1]);
                hm.put("total", "at " + parts[2]);
                hm.put("extra", Integer.toString(flags[0]));
                hm.put("time", parts[2]);
                aList.add(hm);
                txtView.setVisibility(View.GONE);
            }


        } catch (Exception e) {
            Log.e("Exception2:", e.getMessage());
            List<HashMap<String, String>> aList = new ArrayList<HashMap<String, String>>();
            HashMap<String, String> hm = new HashMap<String, String>();
            hm.put("txt", "No news yet");
            //  hm.put("total", "Total steps : " + parts[1] + " Extra Steps: " + parts[2]);
            hm.put("total", "Please check in a while");
            hm.put("extra", Integer.toString(flags[1]));
            aList.add(hm);


        }
    }

    public void createNewsFeed() {
        //   aList = new ArrayList<HashMap<String, String>>();


            try {
                Collections.sort(aList,new Comparator<HashMap<String,String>>(){
                    public int compare(HashMap<String,String> mapping1,HashMap<String,String> mapping2){
                        return mapping2.get("time").compareTo(mapping1.get("time"));
                    }

                });

            Log.e("came", "came here 1");

            String[] from = {"extra", "txt", "total"};
            // Ids of views in listview_layout
            int[] to = {R.id.flag, R.id.txt, R.id.cur};

            SimpleAdapter adapter = new SimpleAdapter(getActivity(), aList, R.layout.listview_layout, from, to);

            ListView listView = (ListView) rootView.findViewById(R.id.challenegeList);

            listView.setAdapter(adapter);

        } catch (Exception e) {

        }
    }


    public void sortList()
    {

    }


}

