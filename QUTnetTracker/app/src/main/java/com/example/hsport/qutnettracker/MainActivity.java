package com.example.hsport.qutnettracker;

import android.Manifest;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.PersistableBundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;

import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hsport.qutnettracker.Tasks.AddCompletedChallngestask;
import com.example.hsport.qutnettracker.Tasks.AddStepDataTask;
import com.example.hsport.qutnettracker.Tasks.ChallengeCountNotifyTask;
import com.example.hsport.qutnettracker.Tasks.ChallengeCountTask;
import com.example.hsport.qutnettracker.Tasks.ChallengesTask;
import com.example.hsport.qutnettracker.Tasks.GMapV2DirectionAsyncTask;
import com.example.hsport.qutnettracker.Tasks.InsertTask;
import com.example.hsport.qutnettracker.Tasks.LoginTask;
import com.example.hsport.qutnettracker.Tasks.SearchExtraStepsTask;
import com.example.hsport.qutnettracker.Tasks.SearchTotalStepsTask;
import com.example.hsport.qutnettracker.Tasks.SearchUserIDTask;
import com.example.hsport.qutnettracker.Tasks.updateChalllengeCountTask;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.FitnessStatusCodes;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataSource;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.data.Subscription;
import com.google.android.gms.fitness.data.Value;
import com.google.android.gms.fitness.request.DataDeleteRequest;
import com.google.android.gms.fitness.request.DataReadRequest;
import com.google.android.gms.fitness.request.DataSourcesRequest;
import com.google.android.gms.fitness.request.DataUpdateRequest;
import com.google.android.gms.fitness.request.OnDataPointListener;
import com.google.android.gms.fitness.request.SensorRequest;
import com.google.android.gms.fitness.result.DailyTotalResult;
import com.google.android.gms.fitness.result.DataReadResult;
import com.google.android.gms.fitness.result.DataSourcesResult;
import com.google.android.gms.fitness.result.ListSubscriptionsResult;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;

import org.w3c.dom.Document;

import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import me.leolin.shortcutbadger.ShortcutBadger;

import static java.text.DateFormat.getDateInstance;
import static java.text.DateFormat.getTimeInstance;

public class MainActivity extends AppCompatActivity implements AsyncResponse, ResultCallback<Status>, NavigationView.OnNavigationItemSelectedListener, OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener, OnDataPointListener {


    public static final String TAG = "BasicSensorsApi";
    private static GoogleMap mMap;
    public static final String SAMPLE_SESSION_NAME = "Afternoon run";
    private static final String DATE_FORMAT = "yyyy.MM.dd HH:mm:ss";
    private static final int REQUEST_OAUTH = 1;
    private static final String AUTH_PENDING = "auth_state_pending";
    private static boolean authInProgress = false;
    private static BlockLocation destinationBlock = null;
    private static final int ERROR_DIALOG_REQUEST = 9001;
    private static final int RC_SIGN_IN = 9001;
    private GoogleApiClient mLocationClient;
    public static GoogleApiClient mGoogleApiClient;
    public static GoogleApiClient mClient;
    private boolean mIntentInProgress;
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    private static Map<String, String> map = new HashMap<String, String>();
    // map.put("name", "demo");

    private static List<Map<String, String>> data = new ArrayList<>();

    public static int badgeCount;
    public static int count = 0;
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;
    private static boolean logedIn = false;
    private boolean mShouldResolve;
    private static boolean notificationSent;

    private ConnectionResult connectionResult;

    private static BlockLocation[] ALLBLOCKLOCATIONS;

    private static final int PROFILE_PIC_SIZE = 400;
    private TextView mStatusTextView;
    private ProgressDialog mProgressDialog;
    private SearchUserIDTask searchUserIDTask;
    private SearchExtraStepsTask searchExtraStepsTask;
    private SearchTotalStepsTask searchTotalStepsTask;
    private static LatLng destPosition;

    public static int ID;
    public static int n = 0;
    private OnDataPointListener mListener;

    NavigationView navigationView = null;
    Toolbar toolbar = null;
    //private GoogleMap mMap;
    //since this class extends from AppCompactActivity we created a SupportMapFragment, else we need to create a MapFragment
    SupportMapFragment supportMapFragment;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    private LocationRequest mLocationRequest;
    public static Location curLocation;

    private SignInButton signInButton;
    private TextView blankScreen;
    private Button signOutButton;
    //  private TextView tvName, tvMail, tvNotSignedIn;


    private Person person;
    private static String personName;
    private String personPhotoUrl;
    private String email;


    private static int firstTime = 1;
    public static String Val1;

    //These are for geo fences

    private PendingIntent mPendingIntent;

    /**
     * Geofences Array
     */
    ArrayList<Geofence> mGeofences = new ArrayList<>();

    /**
     * Geofence Coordinates
     */
    ArrayList<LatLng> mGeofenceCoordinates;

    /**
     * Geofence Radius'
     */
    ArrayList<Integer> mGeofenceRadius;

    /**
     * Geofence Store
     */
    private GeofenceStore mGeofenceStore;

    private LoginTask loginTask;
    private AddCompletedChallngestask addCompletedChallngestask;


    public static String curGeoID;
    public static BlockLocation curGeoBlock;
    public static BlockLocation nearGeoBlock;


    private ResultCallback<Status> mSubscribeResultCallback;
    private ResultCallback<Status> mCancelSubscriptionResultCallback;
    private ResultCallback<ListSubscriptionsResult> mListSubscriptionsResultCallback;
    public Date dte;

    public SimpleDateFormat sdf22;
    public static int stepCount;
    public static int extraStepCount = -1;
    public static int totalStepCount = -1;
    public static int setInternet;
    public static boolean extraStepCountok = false;

    public static String today;
    // public static  extraStepCount=-1;

    public static int challengeInProgress = 0;

    public static String menuFragment = "";

    public static FragmentManager sfm;

    SharedPreferences.Editor editor;

    SharedPreferences prefs;

    public static LocationManager manager;

    @Override
    public void onResult(Status result) {
        if (result.isSuccess()) {
            Log.v("Result", "Success!");
        } else if (result.hasResolution()) {
            // TODO Handle resolution
        } else if (result.isCanceled()) {
            Log.v("Result", "Canceled");
        } else if (result.isInterrupted()) {
            Log.v("Result", "Interrupted");
        } else {

        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        curGeoID = "12A Wilton";
        signInButton = (SignInButton) findViewById(R.id.sign_in_button);
        blankScreen = (TextView) findViewById(R.id.blanktxt);


        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -0);
        today = dateFormat.format(cal.getTime());
        //   signOutButton = (Button) findViewById(R.id.sign_out_button);


        //  SignInButton button = (SignInButton) findViewById(R.id.sign_in_button);
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSignInClicked();
                // do something
            }
        });


        if (savedInstanceState != null) {
            authInProgress = savedInstanceState.getBoolean(AUTH_PENDING);
        }

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Plus.API)
                .addApi(LocationServices.API)
                .addApi(Fitness.HISTORY_API)
                .addApi(Fitness.SENSORS_API)
                .addApi(Fitness.RECORDING_API)
                .addScope(new Scope(Scopes.FITNESS_ACTIVITY_READ))
                .addScope(new Scope(Scopes.FITNESS_ACTIVITY_READ_WRITE))
                .addScope(Fitness.SCOPE_LOCATION_READ)
                .addScope(Fitness.SCOPE_ACTIVITY_READ)
                .addScope(Fitness.SCOPE_BODY_READ_WRITE)
                .addScope(Plus.SCOPE_PLUS_LOGIN)
                .addConnectionCallbacks(this)
                .enableAutoManage(this, 0, this)
                .build();


        supportMapFragment = SupportMapFragment.newInstance();
        supportMapFragment.getMapAsync(this);


        //  checkForChallenges()


        //setExtraSteps();

//        try {
//            if ((menuFragment.equals("myMenuItem"))) {
//
//                //this is to stop the settings fragment from loading when a scahhleneg is accepted
//            } else {
//                Settings_Fragment settings_fragment = new Settings_Fragment();
//                android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
//                fragmentTransaction.replace(R.id.fragment_container, settings_fragment);
//                fragmentTransaction.commit();
//            }
//        } catch (Exception E) {
//            Log.d("trigered", "First Time");
//        }
//        curGeoID = getCurrentGeoID();


//
//        NewsFragment newsFeed = new NewsFragment();
//        android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
//        fragmentTransaction.replace(R.id.fragment_container, newsFeed);
//        fragmentTransaction.commit();


        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);


        navigationView.setNavigationItemSelectedListener(this);


        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();


        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000)        // 10 seconds, in milliseconds
                .setFastestInterval(1 * 1000); // 1 second, in milliseconds

        if (savedInstanceState != null) {
            authInProgress = savedInstanceState.getBoolean(AUTH_PENDING);
        }

        mSubscribeResultCallback = new ResultCallback<Status>() {
            @Override
            public void onResult(@NonNull Status status) {
                if (status.isSuccess()) {
                    if (status.getStatusCode() == FitnessStatusCodes.SUCCESS_ALREADY_SUBSCRIBED) {
                        Log.e("RecordingAPI", "Already subscribed to the Recording API");
                    } else {
                        Log.e("RecordingAPI", "Subscribed to the Recording API");
                    }
                }
            }
        };

        mCancelSubscriptionResultCallback = new ResultCallback<Status>() {
            @Override
            public void onResult(@NonNull Status status) {
                if (status.isSuccess()) {
                    Log.e("RecordingAPI", "Canceled subscriptions!");
                } else {
                    // Subscription not removed
                    Log.e("RecordingAPI", "Failed to cancel subscriptions");
                }
            }
        };

        mListSubscriptionsResultCallback = new ResultCallback<ListSubscriptionsResult>() {
            @Override
            public void onResult(@NonNull ListSubscriptionsResult listSubscriptionsResult) {
                for (Subscription subscription : listSubscriptionsResult.getSubscriptions()) {
                    DataType dataType = subscription.getDataType();
                    Log.e("RecordingAPI", dataType.getName());
                    for (Field field : dataType.getFields()) {
                        Log.e("RecordingAPI", field.toString());
                    }
                }
            }
        };

        dte = new Date();
        sdf22 = new SimpleDateFormat("yyyy/MM/dd");


        editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
        prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);

        editor.putInt("extrastep", extraStepCount);
        editor.putString("today", sdf22.format(dte));
        editor.apply();

        LatLng Ablock = new LatLng(-27.475876, 153.028036);
        LatLng Bblock = new LatLng(-27.476187, 153.028455);


        LatLng Eblock = new LatLng(-27.476586, 153.028239);


        LatLng Fblock = new LatLng(-27.476313, 153.027419);
        LatLng Gblock = new LatLng(-27.476530, 153.027681);
        LatLng Hblock = new LatLng(-27.476829, 153.027869);
        LatLng Oblock = new LatLng(-27.477795, 153.028325);

        LatLng Pblock = new LatLng(-27.478072, 153.029051);

        LatLng Qblock = new LatLng(-27.477167, 153.027541); //50
        LatLng Rblock = new LatLng(-27.476843, 153.027232);

        //   LatLng Sblock = new LatLng(-27.477336, 153.027139);
        LatLng Sblock = new LatLng(-27.477398, 153.027019);
        //  27.477433,153.027605

        LatLng Yblock = new LatLng(-27.477651, 153.029771);


        LatLng Zblock = new LatLng(-27.477840, 153.027561);
        LatLng Cblock = new LatLng(-27.478409, 153.027902);
        LatLng Dblock = new LatLng(-27.476076, 153.027712);
        LatLng Vblock = new LatLng(-27.477014, 153.028385);
        LatLng Mblock = new LatLng(-27.477405, 153.027907);
        LatLng Nblock = new LatLng(-27.477322, 153.029086);


        //LatLng HomeBblock = new LatLng(-27.489722, 153.034608);

        // LatLng Tblock = new LatLng(-27.489150, 153.036348);
       // LatLng Tblock = new LatLng(-27.489571, 153.035671);
        LatLng Xblock = new LatLng(-27.477313, 153.029940);

//
//        LatLng Entrance1 = new LatLng(-27.479232, 153.028374);
//        LatLng Entrance2 = new LatLng(-27.476505, 153.026487);
//        LatLng Entrance3 = new LatLng(-27.475058, 153.027533);


        LatLng[] nearByR = {Sblock, Qblock, Hblock, Vblock, Zblock, Dblock, Ablock, Gblock, Xblock, Eblock};
        LatLng[] nearByQ = {Sblock, Mblock, Hblock, Vblock, Zblock, Dblock, Ablock, Xblock, Rblock, Eblock};
        //LatLng[] nearByHome = {Tblock, Pblock};
        LatLng[] nearByF = {Dblock, Ablock, Gblock, Hblock, Eblock, Cblock, Oblock, Vblock, Sblock, Rblock};
        LatLng[] nearByG = {Eblock, Vblock, Hblock, Qblock, Mblock, Oblock, Zblock, Pblock, Cblock, Sblock};
        LatLng[] nearByH = {Vblock, Qblock, Sblock, Mblock, Zblock, Eblock, Gblock, Yblock, Xblock, Pblock};
        LatLng[] nearByE = {Dblock, Ablock, Qblock, Mblock, Hblock, Zblock, Cblock, Xblock, Pblock, Vblock};
        LatLng[] nearByO = {Zblock, Cblock, Mblock, Vblock, Pblock, Xblock, Ablock, Bblock, Rblock, Qblock};
        LatLng[] nearByP = {Yblock, Nblock, Xblock, Cblock, Vblock, Ablock, Sblock, Hblock, Mblock, Rblock};
        LatLng[] nearByB = {Eblock, Dblock, Gblock, Hblock, Vblock, Ablock, Xblock, Yblock, Pblock, Mblock};

        LatLng[] nearByA = {Dblock, Bblock, Fblock, Gblock, Hblock, Zblock, Rblock, Xblock, Pblock, Cblock};
        LatLng[] nearByY = {Pblock, Nblock, Xblock, Vblock, Oblock, Ablock, Qblock, Sblock, Eblock, Cblock};
        LatLng[] nearByS = {Mblock, Vblock, Hblock, Oblock, Rblock, Ablock, Xblock, Pblock, Gblock, Eblock};


        LatLng[] nearByZ = {Cblock, Oblock, Vblock, Sblock, Rblock, Pblock, Xblock, Yblock, Gblock, Dblock};
        LatLng[] nearByC = {Oblock, Zblock, Pblock, Mblock, Sblock, Ablock, Bblock, Xblock, Rblock, Qblock};
        LatLng[] nearByD = {Ablock, Bblock, Hblock, Qblock, Sblock, Xblock, Pblock, Vblock, Rblock, Mblock};
        LatLng[] nearByV = {Hblock, Mblock, Sblock, Pblock, Oblock, Xblock, Ablock, Bblock, Cblock, Dblock};
        LatLng[] nearByM = {Zblock, Rblock, Hblock, Pblock, Cblock, Xblock, Ablock, Bblock, Gblock, Sblock};
        LatLng[] nearByN = {Pblock, Yblock, Xblock, Oblock, Cblock, Dblock, Ablock, Bblock, Gblock, Sblock};
     //   LatLng[] nearByHome = {Tblock, Zblock, Hblock, Pblock, Cblock, Dblock, Ablock, Gblock, Hblock, Eblock};
        LatLng[] nearByX = {Pblock, Yblock, Nblock, Oblock, Cblock, Dblock, Ablock, Gblock, Hblock, Eblock};


        ALLBLOCKLOCATIONS = new BlockLocation[]{

                new BlockLocation(Rblock, "R Block", 35, nearByR),
                new BlockLocation(Qblock, "Q Block", 25, nearByQ),
               // new BlockLocation(HomeBblock, "12A Wilton", 50, nearByHome),
                // new BlockLocation(new LatLng(-27.476824, 153.027146), "Chandler, AZ", 50, nearByQ),
                new BlockLocation(Fblock, "F Block", 25, nearByF),
                new BlockLocation(Gblock, "G Block", 25, nearByG),
                new BlockLocation(Hblock, "H Block", 25, nearByH),
                new BlockLocation(Eblock, "E Block", 25, nearByE),
                new BlockLocation(Oblock, "O Block", 40, nearByO),
                new BlockLocation(Pblock, "P Block", 40, nearByP),
                //  new BlockLocation(new LatLng(-27.478086, 153.029202), "Government Office", 50, nearByG),
                new BlockLocation(Bblock, "B Block", 35, nearByB),
                new BlockLocation(Ablock, "A Block", 25, nearByA),
                new BlockLocation(Yblock, "Y Block", 25, nearByY),
                new BlockLocation(Sblock, "S Block", 46, nearByS),
               // new BlockLocation(Tblock, "T Block", 20, nearByF),
                new BlockLocation(Xblock, "X Block", 35, nearByX),
//                new BlockLocation(Entrance1, "Entrance1", 50, nearByF),
//                new BlockLocation(Entrance2, "Entrance2", 50, nearByF),
//                new BlockLocation(Entrance3, "Entrance3", 50, nearByF),
                new BlockLocation(Zblock, "Z Block", 35, nearByZ),
                new BlockLocation(Cblock, "C Block", 40, nearByC),
                new BlockLocation(Dblock, "D Block", 30, nearByD),
                new BlockLocation(Vblock, "V Block",35, nearByV),
                new BlockLocation(Mblock, "M Block", 30, nearByM),
                new BlockLocation(Nblock, "N Block", 35, nearByN),
                // new BlockLocation(OldGblock, "Old Government Block", 50, nearByP),

        };


        for (int i = 0; i < ALLBLOCKLOCATIONS.length; i++) {
            mGeofences.add(new Geofence.Builder()
                    .setRequestId(ALLBLOCKLOCATIONS[i].getmId())
                    // The coordinates of the center of the geofence and the radius in meters.
                    .setCircularRegion(ALLBLOCKLOCATIONS[i].getmLatLng().latitude, ALLBLOCKLOCATIONS[i].getmLatLng().longitude, ALLBLOCKLOCATIONS[i].getMradius())
                    .setExpirationDuration(Geofence.NEVER_EXPIRE)
                    // Required when we use the transition type of GEOFENCE_TRANSITION_DWELL
                    .setLoiteringDelay(30000)
                    .setTransitionTypes(
                            Geofence.GEOFENCE_TRANSITION_ENTER
                                    | Geofence.GEOFENCE_TRANSITION_DWELL
                                    | Geofence.GEOFENCE_TRANSITION_EXIT).build());





        }

        mGeofenceStore = new GeofenceStore(this, mGeofences);


        getSupportActionBar().setTitle("News Feed");

        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationReceiver, new IntentFilter("some_custom_id"));

        //   displayMap();


        // createGeoFences("init");

        NewsFragment newsFeed = new NewsFragment();
        android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, newsFeed);
        fragmentTransaction.commit();


//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                while (true) {
//                    try {
//
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//
//                    // someObject.doSomething();
//                }
//            }
//        }).start();

        checkForChallengeslAsynchronousTask();
        Snackbar.make(findViewById(R.id.main_activity_view), "Todays Extra Steps " + extraStepCount, Snackbar.LENGTH_LONG);

//        int badgeCount = 1;
//        ShortcutBadger.applyCount(this, badgeCount); //for 1.1.4
        //  ShortcutBadger.with(getApplicationContext()).count(badgeCount); //for 1.1.3

    }

    public void checkForChallengeslAsynchronousTask() {
        final Handler handler = new Handler();
        Timer timer = new Timer();
        TimerTask doAsynchronousTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        try {
                            //if ((menuFragment == null)) {
                            checkForChallenges();
                            Log.d("Checking", "Checking for challenges");
                            if (badgeCount > 0)
                                ShortcutBadger.applyCount(getApplication(), badgeCount); //for 1.1.4
                            else
                                ShortcutBadger.removeCount(getApplication());
                            // }
                        } catch (Exception e) {
                            e.printStackTrace();
                            // TODO Auto-generated catch block
                        }
                    }
                });
            }
        };
        timer.schedule(doAsynchronousTask, 0, 20000); //execute in every 50000 ms
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNotificationReceiver);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.

        String title = getString(R.string.app_name);
        // Log.d("Tag", "badu came");
        int id = item.getItemId();
        sfm = getSupportFragmentManager();


        if (supportMapFragment.isAdded())
            sfm.beginTransaction().hide(supportMapFragment).commit();
//
//        if (id == R.id.challenge_progress) {
//            title = "See your Steps";
//            StepCountFragment stepCountFragment = new StepCountFragment();
//            android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
//            fragmentTransaction.replace(R.id.fragment_container, stepCountFragment);
//            fragmentTransaction.commit();
//
//        } else
        if (id == R.id.challenge) {


            title = "Challenge in Progress";
            supportMapFragment = SupportMapFragment.newInstance();
            supportMapFragment.getMapAsync(this);

            int inProgress = getChallengeInProgress();

            if (!supportMapFragment.isAdded()) {

                sfm.beginTransaction().add(R.id.map, supportMapFragment).commit();


            } else if (supportMapFragment.isAdded()) {

                // sfm.beginTransaction().remove(getSupportFragmentManager().findFragmentById(R.id.map)).commit();


                //  sfm.beginTransaction().add(R.id.map, supportMapFragment).commit();

                sfm.beginTransaction().show(supportMapFragment).commit();


            }


        } else if (id == R.id.qutnet_friends) {

            title = "My Challenges";

            ChallengesForMeFragment challengesForMeFragment = new ChallengesForMeFragment();
            android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, challengesForMeFragment);
            fragmentTransaction.commit();


        } else if (id == R.id.offer_challenge) {
            title = "Challenge Friends";


            OfferChallengeFragment offerChallengeFragment = (OfferChallengeFragment) getSupportFragmentManager().findFragmentByTag("offerChallengeFragment");

            if (offerChallengeFragment == null) {
                offerChallengeFragment = OfferChallengeFragment.newInstance(personName);
                android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.fragment_container, offerChallengeFragment);
                fragmentTransaction.commit();
            }


        } else if (id == R.id.team_members) {
            title = "Team Leaderboard";
            TeamMembersFragment teamMembersFragment = new TeamMembersFragment();
            android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, teamMembersFragment);
            fragmentTransaction.commit();


        } else if (id == R.id.newsFeed) {
            title = "News Feed";
            NewsFragment newsFragment = new NewsFragment();
            android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, newsFragment);
            fragmentTransaction.commit();

//
//        } else if (id == R.id.settings) {
//            Settings_Fragment settings_fragment = new Settings_Fragment();
//            android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
//            fragmentTransaction.replace(R.id.fragment_container, settings_fragment);
//            fragmentTransaction.commit();


        } else if (id == R.id.Create_Groups) {
            title = "Create or Join Groups";
            //GroupFragment freindsListFragment = (GroupFragment) getSupportFragmentManager().findFragmentByTag("freindsListFragment");
            GroupFragment groupFragment = GroupFragment.newInstance(personName);


            android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, groupFragment);
            fragmentTransaction.commit();


        } else if (id == R.id.login) {

            if (mGoogleApiClient.isConnected()) {
                Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
                mGoogleApiClient.disconnect();
                signInUI();
            }


        } else if (id == R.id.exit) {

            final AlertDialog.Builder builder = new AlertDialog.Builder(this);

            builder.setMessage("Are you sure you want to exit?")
                    .setCancelable(false)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(final DialogInterface dialog, final int id) {
                            Log.e("Activity", "Clicked on exit");
                            System.exit(0);
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(final DialogInterface dialog, final int id) {
                            dialog.cancel();
                        }
                    });
            final AlertDialog alert = builder.create();
            alert.show();


        }
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);


        return true;
    }


    public void displayMap() {
        supportMapFragment = SupportMapFragment.newInstance();
        supportMapFragment.getMapAsync(this);

        if (!supportMapFragment.isAdded()) {

            sfm.beginTransaction().add(R.id.map, supportMapFragment).commit();


        } else if (supportMapFragment.isAdded()) {

            // sfm.beginTransaction().remove(getSupportFragmentManager().findFragmentById(R.id.map)).commit();


            //  sfm.beginTransaction().add(R.id.map, supportMapFragment).commit();

            sfm.beginTransaction().show(supportMapFragment).commit();


        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        try {
            if (!mGoogleApiClient.isConnected()) {
                Log.e("Activity", "Activity Clint is disconnected");
                mGoogleApiClient.connect();
            }
            // Log.d("Map ready", "Map is raedy called");
            // final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                buildAlertMessageNoGps();

            }


            int inProgress = getChallengeInProgress();


            //   LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);

            //manager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, mLocationRequest);


            curLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);


            if (curLocation == null) {
                Toast.makeText(this, "Location service not available", Toast.LENGTH_SHORT).show();
            } else {
                mMap = googleMap;

                mMap.setPadding(100, 300, 100, 0);
                //   mMap = supportMapFragment.getMap();
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED) {
                    mMap.setMyLocationEnabled(true);

                    //  mMap.getUiSettings().setMyLocationButtonEnabled(true);
                } else {
                    // Show rationale and request permission.
                }


                //  curGeoID = getCurrentGeoID();

                //curGeoBlock = getCurGeoBlock(curGeoID);


                curGeoBlock = getMyGeoFence(curLocation);
                nearGeoBlock = getMyNearby(curGeoBlock.getmId());

                Toast.makeText(getBaseContext(), "My Geo fence " + curGeoBlock.getmId() + "Near By " + nearGeoBlock.getmId() + "n value= " + n, Toast.LENGTH_LONG).show();
            }

            //    curGeoBlock = new BlockLocation(HomeBblock, "12A Wilton", 100, nearByHome);

            if (destinationBlock == null) {
                createGeoFences("init");
                showCurrentLocation(curGeoBlock);
                //   Log.d("triger", "T1");
            } else {
                // Log.d("triger", "T2");
                createGeoFences("challenge");
                showChallenege();
                StartSensors();
                nearGeoBlock = destinationBlock;
                destinationBlock = null;
                //Log.d("desitnation", "became null");
            }


            //This is how you draw the geofence in only your nearby ones


//            for (int i = 0; i < ALLBLOCKLOCATIONS.length; i++) {
//                mMap.addCircle(new CircleOptions().center(ALLBLOCKLOCATIONS[i].getmLatLng())
//                        .radius(ALLBLOCKLOCATIONS[i].getMradius())
//                        .fillColor(0x40ff0000)
//                        .strokeColor(Color.TRANSPARENT).strokeWidth(2));
//
//
//            }

        } catch (Exception e) {
            // Log.e("OnMapReady", e.getMessage());

        }
    }


    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();

    }


//   public void createGeoFencesinitial()
//   {
//
//       mGeofences.add(new Geofence.Builder()
//               .setRequestId(curGeoBlock.getmId())
//               // The coordinates of the center of the geofence and the radius in meters.
//               .setCircularRegion(curGeoBlock.getmLatLng().latitude, curGeoBlock.getmLatLng().longitude, curGeoBlock.getMradius())
//               .setExpirationDuration(Geofence.NEVER_EXPIRE)
//               // Required when we use the transition type of GEOFENCE_TRANSITION_DWELL
//               .setLoiteringDelay(30000)
//               .setTransitionTypes(
//                       Geofence.GEOFENCE_TRANSITION_ENTER
//                               | Geofence.GEOFENCE_TRANSITION_DWELL
//                               | Geofence.GEOFENCE_TRANSITION_EXIT).build());
//
//       LatLng lng = new LatLng(curGeoBlock.getmLatLng().latitude, curGeoBlock.getmLatLng().longitude);
//
//   }


    public void createGeoFences(String extra) {

        //  Log.d("creating", "creating geofences");


        //  BlockLocation[] myNearbyBlockLocations = getNearby(blockID);
        //final BlockLocation myNearbyBlockLocations = getMyNearby(blockID);


        if (extra.equals("init")) {
            //  Log.d("creating", "creating geofences 1");
            mGeofences.add(new Geofence.Builder()
                    .setRequestId(curGeoBlock.getmId())
                    // The coordinates of the center of the geofence and the radius in meters.
                    .setCircularRegion(curGeoBlock.getmLatLng().latitude, curGeoBlock.getmLatLng().longitude, curGeoBlock.getMradius())
                    .setExpirationDuration(Geofence.NEVER_EXPIRE)
                    // Required when we use the transition type of GEOFENCE_TRANSITION_DWELL
                    .setLoiteringDelay(30000)
                    .setTransitionTypes(
                            Geofence.GEOFENCE_TRANSITION_ENTER
                                    | Geofence.GEOFENCE_TRANSITION_DWELL
                                    | Geofence.GEOFENCE_TRANSITION_EXIT).build());

            LatLng lng = new LatLng(curGeoBlock.getmLatLng().latitude, curGeoBlock.getmLatLng().longitude);
//            mMap.addCircle(new CircleOptions().center(lng)
//                    .radius(curGeoBlock.getMradius())
//                    .fillColor(0x40ff0000)
//                    .strokeColor(Color.TRANSPARENT).strokeWidth(2));

        } else if (extra.equals("challenge")) {
            //  Log.d("creating", "creating geofences 2");


            mGeofences.add(new Geofence.Builder()
                    .setRequestId(nearGeoBlock.getmId())
                    // The coordinates of the center of the geofence and the radius in meters.
                    .setCircularRegion(nearGeoBlock.getmLatLng().latitude, nearGeoBlock.getmLatLng().longitude, nearGeoBlock.getMradius())
                    .setExpirationDuration(Geofence.NEVER_EXPIRE)
                    // Required when we use the transition type of GEOFENCE_TRANSITION_DWELL
                    .setLoiteringDelay(30000)
                    .setTransitionTypes(
                            Geofence.GEOFENCE_TRANSITION_ENTER
                                    | Geofence.GEOFENCE_TRANSITION_DWELL
                                    | Geofence.GEOFENCE_TRANSITION_EXIT).build());


            //This was done to accomodate challeges offered by friends

            if (destinationBlock != null) {


                mGeofences.add(new Geofence.Builder()
                        .setRequestId(destinationBlock.getmId())
                        // The coordinates of the center of the geofence and the radius in meters.
                        .setCircularRegion(destinationBlock.getmLatLng().latitude, destinationBlock.getmLatLng().longitude, destinationBlock.getMradius())
                        .setExpirationDuration(Geofence.NEVER_EXPIRE)
                        // Required when we use the transition type of GEOFENCE_TRANSITION_DWELL
                        .setLoiteringDelay(30000)
                        .setTransitionTypes(
                                Geofence.GEOFENCE_TRANSITION_ENTER
                                        | Geofence.GEOFENCE_TRANSITION_DWELL
                                        | Geofence.GEOFENCE_TRANSITION_EXIT).build());


                mMap.addCircle(new CircleOptions().center(destinationBlock.getmLatLng())
                        .radius(destinationBlock.getMradius())
                        .fillColor(0x4000FF00)
                        .strokeColor(Color.TRANSPARENT).strokeWidth(2));


            }

        }


        LocationServices.GeofencingApi.addGeofences(
                mGoogleApiClient,
                getGeofencingRequest(),
                getGeofencePendingIntent()
        ).setResultCallback(this);


        //  final BlockLocation myBlockLocation = getMyGeoFence(curLocation);
        //  final BlockLocation myBlockLocation = curGeoBlock;


        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        //   mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
        mMap.setIndoorEnabled(true);
        // mMap.setMyLocationEnabled(true);
        mMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition position) {

                LatLng lng1 = new LatLng(nearGeoBlock.getmLatLng().latitude, nearGeoBlock.getmLatLng().longitude);


            }
        });

    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        supportMapFragment.onSaveInstanceState(outState);
    }

    public BlockLocation getMyNearby(String mygeofence) {
        LatLng nearBy = null;
        LatLng nearByarr = null;
        // Random random = new Random();
        //This is to get a random number between 1 to 5
        //  int n = random.nextInt(5 - 0) + 0;

        if (n > 9)
            n = 0;
        //  nearByarr[0]=LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        BlockLocation nearbyBlocks = null;
        // Log.d("came here1 ", mygeofence);
        // int i=0;
        for (int i = 0; i < ALLBLOCKLOCATIONS.length; i++) {
            //   Log.d("came here4 ", ALLBLOCKLOCATIONS[i].getmId());
            if (mygeofence.trim().equalsIgnoreCase(ALLBLOCKLOCATIONS[i].getmId())) {


                nearByarr = ALLBLOCKLOCATIONS[i].getMnearby()[n];

//                if (count == 0)
//                    nearByarr = ALLBLOCKLOCATIONS[i].getMnearby()[0];
//                else
//                    nearByarr = ALLBLOCKLOCATIONS[i].getMnearby()[1];

            }
        }
        n++;
        count = 1;

        //   for (int i = 0; i < nearByarr.length; i++) {
        Location targetLocation = new Location("");//provider name is unecessary
        targetLocation.setLatitude(nearByarr.latitude);//your coords of course
        targetLocation.setLongitude(nearByarr.longitude);


        // to get all nearby fences
        // nearbyBlocks = getMyGeoFence(targetLocation);
        nearbyBlocks = getGeofenceFromLocation(targetLocation);
        //To get only one. Should replace this with a random number

        // nearbyBlocks[0] = getMyGeoFence(targetLocation);
        // }

        // Toast.makeText(getBaseContext(), "my Near by =>" + myBlockLocation.getMnearby()[0] + " or " + myBlockLocation.getMnearby()[1], Toast.LENGTH_LONG).show();

        return nearbyBlocks;
        //sned the notification mentioning you ahve a challeneg. And then forward the player to this location if accepted..
        // Log.d("testing",mygeofence);


    }

    public void setN() {
        n++;
    }

    public void setNearByBlockwhenCanceled(String mygeofence) {
        LatLng nearBy = null;
        LatLng nearByarr = null;
        // Random random = new Random();
        //This is to get a random number between 1 to 5
        //  int n = random.nextInt(5 - 0) + 0;

        if (n > 9)
            n = 0;
        //  nearByarr[0]=LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        BlockLocation nearbyBlocks = null;
        // Log.d("came here1 ", mygeofence);
        // int i=0;
        for (int i = 0; i < ALLBLOCKLOCATIONS.length; i++) {
            //   Log.d("came here4 ", ALLBLOCKLOCATIONS[i].getmId());
            if (mygeofence.trim().equalsIgnoreCase(ALLBLOCKLOCATIONS[i].getmId())) {


                nearByarr = ALLBLOCKLOCATIONS[i].getMnearby()[n];

//                if (count == 0)
//                    nearByarr = ALLBLOCKLOCATIONS[i].getMnearby()[0];
//                else
//                    nearByarr = ALLBLOCKLOCATIONS[i].getMnearby()[1];

            }
        }

        count = 1;

        //   for (int i = 0; i < nearByarr.length; i++) {
        Location targetLocation = new Location("");//provider name is unecessary
        targetLocation.setLatitude(nearByarr.latitude);//your coords of course
        targetLocation.setLongitude(nearByarr.longitude);


        // to get all nearby fences
        // nearbyBlocks = getMyGeoFence(targetLocation);
        nearbyBlocks = getGeofenceFromLocation(targetLocation);
        //To get only one. Should replace this with a random number

        // nearbyBlocks[0] = getMyGeoFence(targetLocation);
        // }

        // Toast.makeText(getBaseContext(), "my Near by =>" + myBlockLocation.getMnearby()[0] + " or " + myBlockLocation.getMnearby()[1], Toast.LENGTH_LONG).show();
        nearGeoBlock = nearbyBlocks;
        destinationBlock = nearbyBlocks;

        //sned the notification mentioning you ahve a challeneg. And then forward the player to this location if accepted..
        // Log.d("testing",mygeofence);


    }


    public BlockLocation getGeofenceFromLocation(Location targetLocation) {
        BlockLocation blockLocation = ALLBLOCKLOCATIONS[11];
        try {
            for (int i = 0; i < ALLBLOCKLOCATIONS.length; i++) {

                if (ALLBLOCKLOCATIONS[i].getmLatLng().latitude == targetLocation.getLatitude() && ALLBLOCKLOCATIONS[i].getmLatLng().longitude == targetLocation.getLongitude())
                    blockLocation = ALLBLOCKLOCATIONS[i];
//                Location.distanceBetween(targetLocation.getLatitude(), targetLocation.getLongitude(),
//                        ALLBLOCKLOCATIONS[i].getmLatLng().latitude, ALLBLOCKLOCATIONS[i].getmLatLng().longitude, distance);
//
//                if (distance[0] < ALLBLOCKLOCATIONS[i].getMradius()) {
//                    myBlock = ALLBLOCKLOCATIONS[i];
//                }

            }


        } catch (Exception e) {

            Toast.makeText(this, "You are not in QUT premises", Toast.LENGTH_SHORT);
            // return myBlock;
        }
        return blockLocation;
    }

    public BlockLocation[] getNearby(String mygeofence) {
        LatLng nearBy = null;
        LatLng[] nearByarr = new LatLng[2];
        //  nearByarr[0]=LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        BlockLocation[] nearbyBlocks = new BlockLocation[2];
        //  Log.d("came here1 ", mygeofence);
        // int i=0;
        for (int i = 0; i < ALLBLOCKLOCATIONS.length; i++) {
            //    Log.d("came here4 ", ALLBLOCKLOCATIONS[i].getmId());
            if (mygeofence.trim().equalsIgnoreCase(ALLBLOCKLOCATIONS[i].getmId())) {
                nearByarr[0] = ALLBLOCKLOCATIONS[i].getMnearby()[0];
                nearByarr[1] = ALLBLOCKLOCATIONS[i].getMnearby()[1];

                // nearbyBlocks[i]=ALLBLOCKLOCATIONS[i];

            }
        }

        for (int i = 0; i < nearByarr.length; i++) {
            Location targetLocation = new Location("");//provider name is unecessary
            targetLocation.setLatitude(nearByarr[i].latitude);//your coords of course
            targetLocation.setLongitude(nearByarr[i].longitude);


            // to get all nearby fences
            nearbyBlocks[i] = getMyGeoFence(targetLocation);
            //To get only one. Should replace this with a random number

            // nearbyBlocks[0] = getMyGeoFence(targetLocation);
        }

        // Toast.makeText(getBaseContext(), "my Near by =>" + myBlockLocation.getMnearby()[0] + " or " + myBlockLocation.getMnearby()[1], Toast.LENGTH_LONG).show();

        return nearbyBlocks;
        //sned the notification mentioning you ahve a challeneg. And then forward the player to this location if accepted..
        // Log.d("testing",mygeofence);


    }


    public BlockLocation getMyGeoFence(Location location) {
        BlockLocation myBlock = ALLBLOCKLOCATIONS[11];


        // BlockLocation myBlock = null;

        float[] distance = new float[2];

        try {
            for (int i = 0; i < ALLBLOCKLOCATIONS.length; i++) {
                Location.distanceBetween(location.getLatitude(), location.getLongitude(),
                        ALLBLOCKLOCATIONS[i].getmLatLng().latitude, ALLBLOCKLOCATIONS[i].getmLatLng().longitude, distance);

                if (distance[0] < ALLBLOCKLOCATIONS[i].getMradius()) {
                    myBlock = ALLBLOCKLOCATIONS[i];
                }

            }


        } catch (Exception e) {

            Toast.makeText(this, "You are not in QUT premises", Toast.LENGTH_SHORT);
            // return myBlock;
        }


        return myBlock;
    }

    public void showChallenege() {
        if (curLocation == null) {
            Toast.makeText(this, "Location service not available", Toast.LENGTH_SHORT).show();
        } else {


            //Need to change 0 by a static random number which is common with nearbyBlockLoction

            // destPosition = new LatLng(destinationBlock.getMnearby()[0].latitude, destinationBlock.getMnearby()[0].longitude);
            destPosition = new LatLng(destinationBlock.getmLatLng().latitude, destinationBlock.getmLatLng().longitude);
            LatLng latLng = new LatLng(curGeoBlock.getmLatLng().latitude, curGeoBlock.getmLatLng().longitude);
            CameraUpdate update = CameraUpdateFactory.newLatLngZoom(latLng, 15);
            Marker perth = mMap.addMarker(new MarkerOptions()
                    .position(destPosition)
                    .draggable(true));
            mMap.animateCamera(update);


            GMapV2Direction md = new GMapV2Direction();

            LatLng sourcePosition = new LatLng(curLocation.getLatitude(), curLocation.getLongitude());
            // LatLng destPosition =new LatLng(-27.477705, 153.028326);


            //    Log.d("dest", "destination " + destinationBlock.getmId());
            //  Log.d("dest", "Nearby " + nearGeoBlock.getmId());
            // route(latLng, destPosition, GMapV2Direction.MODE_WALKING);


        }


    }


    private void updateTextViewWithStepCounter(final int numberOfSteps) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //  Log.d("Steps", "5");
                Toast.makeText(getBaseContext(), "On Datapoint!",
                        Toast.LENGTH_SHORT);
                String step = Integer.toString(numberOfSteps);
                //Log.d("Steps", "Walking " + step);


            }
        });
    }


    public void showCurrentLocation(BlockLocation destinationBlock) {
        if (curLocation == null) {
            Toast.makeText(this, "Location service not available", Toast.LENGTH_SHORT).show();
        } else {
            LatLng latLng = new LatLng(curLocation.getLatitude(), curLocation.getLongitude());
            CameraUpdate update = CameraUpdateFactory.newLatLngZoom(latLng, 15);
//            Marker perth = mMap.addMarker(new MarkerOptions()
//                    .position(latLng)
//                    .draggable(true));
            mMap.animateCamera(update);

        }
//        Log.d("Debug","Fiteness Building");
//        buildFitnessClient();
//        Log.d("Debug", "Fiteness Building");
    }

    protected void route(LatLng sourcePosition, LatLng destPosition, String mode) {
        final Handler handler = new Handler() {
            public void handleMessage(Message msg) {
                try {
                    Document doc = (Document) msg.obj;

                    GMapV2Direction md = new GMapV2Direction();

                    ArrayList<LatLng> directionPoint = md.getDirection(doc);

                    //PolylineOptions rectLine = new PolylineOptions().width(15).color(getActivity().getResources().getColor(R.color.magoo_user_base_color));
                    PolylineOptions rectLine = new PolylineOptions().width(15).color(
                            Color.BLUE);

                    for (int i = 0; i < directionPoint.size(); i++) {
                        rectLine.add(directionPoint.get(i));
                    }
                    Polyline polylin = mMap.addPolyline(rectLine);
                    md.getDurationText(doc);
                    //     Log.d("duration", md.getDurationText(doc));
                    //   Log.d("Distance", md.getDistanceText(doc));
                    //  Log.d("duration",md.getDurationText(doc));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            ;
        };
        new GMapV2DirectionAsyncTask(handler, sourcePosition, destPosition, GMapV2Direction.MODE_WALKING).execute();
    }

    //These three methods are uused when getting the map to get the current location.
    // Added automatically when implementing the interface
    @Override
    public void onConnected(Bundle bundle) {
        // signOutUI();
        mGoogleApiClient.connect();
        try {
            //  Toast.makeText(this, "Ready to map", Toast.LENGTH_SHORT).show();
            Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            if (location == null) {
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            } else {

                handleNewLocation(location);
            }
            ;

            mShouldResolve = false;

            if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {
                person = Plus.PeopleApi
                        .getCurrentPerson(mGoogleApiClient);
                personName = person.getDisplayName();
                personPhotoUrl = person.getImage().getUrl();
                email = Plus.AccountApi.getAccountName(mGoogleApiClient);


                boolean status = checkStatus(personName);


                if (status == true)
                    Log.d("Status", "true");
                else
                    new InsertTask(this, person).execute(personName, personPhotoUrl, "user");

                menuFragment = getIntent().getStringExtra("mapFragment");
                final FragmentManager sfm1 = getSupportFragmentManager();

                //SO that it will not pop up notifications even after it's seen. (the notification reloads the main activty)
                if (menuFragment == null)
                    checkForChallenges();

                //    Log.d("badu came", "1");


                //   Log.d("badu came2",menuFragment);
                if (menuFragment != null) {

                    if (menuFragment.equals("myOfferedChallenge")) {
                        getSupportActionBar().setTitle("Challenge in progress");
                        setChallengeInProgress(1);
                        notificationSent = true;


                        if (supportMapFragment.isAdded())
                            sfm1.beginTransaction().hide(supportMapFragment).commit();

                        if (!supportMapFragment.isAdded()) {
                            sfm1.beginTransaction().add(R.id.map, supportMapFragment).commit();
                            //  goToLocation(-27.476591, 153.028118);
                        } else
                            sfm1.beginTransaction().show(supportMapFragment).commit();
                    }


                    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case DialogInterface.BUTTON_POSITIVE:

                                    if (menuFragment.equals("myMenuItem")) {
                                        setChallengeInProgress(1);
                                        notificationSent = true;


                                        if (supportMapFragment.isAdded())
                                            sfm1.beginTransaction().hide(supportMapFragment).commit();

                                        if (!supportMapFragment.isAdded()) {
                                            sfm1.beginTransaction().add(R.id.map, supportMapFragment).commit();
                                            //  goToLocation(-27.476591, 153.028118);
                                        } else
                                            sfm1.beginTransaction().show(supportMapFragment).commit();


                                        //new CountDownTimer(1200000, 1000) {
                                        new CountDownTimer(1200000, 1000) {

                                            public void onTick(long millisUntilFinished) {
                                                Log.e("Timing out", "Time is ticking");
                                            }

                                            public void onFinish() {
                                                //finish() // finish ActivityB
                                                Log.e("Timing out", "Timingout");
                                                extraStepCountok = false;
                                                setChallengeInProgress(0);
                                                finish();
                                                startActivity(getIntent());

//                                                Intent notificationIntent = new Intent(this);
//                                                notificationIntent.putExtra("mapFragment", "myOfferedChallengeTimedout");
//                                                // Construct a task stack.
//                                                TaskStackBuilder stackBuilder = TaskStackBuilder.create(getActivity());
//                                                // Add the main Activity to the task stack as the parent.
//                                                stackBuilder.addParentStack(MainActivity.class);
//                                                // Push the content Intent onto the stack.
//                                                stackBuilder.addNextIntent(notificationIntent);
//
//                                                startActivity(notificationIntent);
                                            }
                                        }.start();

                                    } else if (menuFragment.equals("favoritesMenuItem")) {

                                        // new updateStatusTask().execute();

                                        ChallengesForMeFragment settings_fragment = new ChallengesForMeFragment();
                                        android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                                        fragmentTransaction.replace(R.id.fragment_container, settings_fragment);
                                        fragmentTransaction.commit();

                                    }

                                    break;

                                //StartEtraStepCOunt()
                                case DialogInterface.BUTTON_NEGATIVE:
                                    if (menuFragment.equals("favoritesMenuItem")) {
                                        // menuFragment = "canceled";
                                    }
                                    //No button clicked
                                    break;
                            }
                        }
                    };

                    if (menuFragment.equals("favoritesMenuItem")) {
                        if (firstTime == 1) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(this);
                            builder.setMessage("Do you want to see all your challenges?").setPositiveButton("Yes", dialogClickListener)
                                    .setNegativeButton("No", dialogClickListener).show();
                            firstTime = 0;
                        }

                    } else if (menuFragment.equals("myMenuItem")) {
                        if (firstTime == 1) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(this);
                            builder.setMessage("Are you sure you want to accept the challenge?").setPositiveButton("Yes", dialogClickListener)
                                    .setNegativeButton("No", dialogClickListener).show();
                            firstTime = 0;
                        }
                    } else if (menuFragment.equals("myOfferedChallengeTimedout")) {

                        getSupportActionBar().setTitle("News Feed");
                        //}

                        NewsFragment newsFragment = new NewsFragment();
                        android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                        fragmentTransaction.replace(R.id.fragment_container, newsFragment);
                        fragmentTransaction.commit();
                    }

                }


                String value = getIntent().getStringExtra("VALUE");
                if (value != null) {
                    Log.e("Activity", "Activity is starting 2");
                    if (value.equalsIgnoreCase("selected")) {
                        Log.e("Activity", "Activity is starting 3");

                        checkForChallenges();


                        getSupportActionBar().setTitle("News Feed");
                        //}

                        NewsFragment newsFragment = new NewsFragment();
                        android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                        fragmentTransaction.replace(R.id.fragment_container, newsFragment);
                        fragmentTransaction.commit();

//
//                        supportMapFragment = SupportMapFragment.newInstance();
//                        supportMapFragment.getMapAsync(this);
//
//                        if (!supportMapFragment.isAdded()) {
//
//                            sfm1.beginTransaction().add(R.id.map, supportMapFragment).commit();
//
//
//                        } else if (supportMapFragment.isAdded()) {
//
//                            // sfm.beginTransaction().remove(getSupportFragmentManager().findFragmentById(R.id.map)).commit();
//
//
//                            //  sfm.beginTransaction().add(R.id.map, supportMapFragment).commit();
//
//                            sfm1.beginTransaction().show(supportMapFragment).commit();
//
//
//                        }


//                        if (supportMapFragment.isAdded())
//                            Log.e("Activity", "Activity is starting 4");
//                            sfm1.beginTransaction().hide(supportMapFragment).commit();
//
//                        if (!supportMapFragment.isAdded()) {     Log.e("Activity", "Activity is starting 5");
//                            sfm1.beginTransaction().add(R.id.map, supportMapFragment).commit();
//                            //  goToLocation(-27.476591, 153.028118);
//                        } else
//                            sfm1.beginTransaction().show(supportMapFragment).commit();
//                        Log.e("Activity", "Activity is starting 6");

                    }

                }


                searchUserIDTask = (SearchUserIDTask) new SearchUserIDTask(MainActivity.this).execute(personName);

                Log.d("getting", "getting");
                if (extraStepCount == -1)
                    Snackbar.make(findViewById(R.id.main_activity_view), "Todays Extra Steps " + 0, Snackbar.LENGTH_LONG).show();
                else
                    Snackbar.make(findViewById(R.id.main_activity_view), "Todays Extra Steps " + extraStepCount, Snackbar.LENGTH_LONG).show();


                TextView tv1 = (TextView) findViewById(R.id.textViewSummay);
                TextView tv3 = (TextView) findViewById(R.id.textViewSummay2);

                if (extraStepCount > 0) {

                    tv1.setText("Extra Steps " + extraStepCount);
                    tv3.setText("Total Steps " + Val1);
                }

                TextView tv2 = (TextView) findViewById(R.id.username);
                tv2.setText(personName.toString());


                ImageView imgView = (ImageView) findViewById(R.id.imageViewperson);

//                URL newurl = new URL(personPhotoUrl);
//                Bitmap mIcon_val = BitmapFactory.decodeStream(newurl.openConnection().getInputStream());
//                imgView.setImageBitmap(mIcon_val);


                // ImageView img= (ImageView) findViewById(R.id.image);
                // imgView.setImageResource(personPhotoUrl);


                // Snackbar.make(findViewById(R.id.main_activity_view), "Todays Extra Steps ", Snackbar.LENGTH_LONG).show();
//                Toast.makeText(getApplicationContext(),
//                        "You are Logged In " + personName, Toast.LENGTH_LONG).show();
            } else {

//                Toast.makeText(getApplicationContext(),
//                        "Couldnt Get the Person Info", Toast.LENGTH_SHORT).show();
            }


            Calendar cal = Calendar.getInstance();
            Date now = new Date();
            cal.setTime(now);
            long endTime = cal.getTimeInMillis();
            cal.set(Calendar.HOUR_OF_DAY, 0);
            cal.set(Calendar.MINUTE, 0);
            cal.set(Calendar.SECOND, 0);
            long startTime = cal.getTimeInMillis();

//            final DataReadRequest readRequest = new DataReadRequest.Builder()
//                    .read(DataType.TYPE_STEP_COUNT_DELTA)
//                    .setTimeRange(startTime, endTime, TimeUnit.MILLISECONDS)
//                    .build();

//            DataReadResult dataReadResult =
//                    Fitness.HistoryApi.readData(mGoogleApiClient, readRequest).await(1, TimeUnit.MINUTES);


            //    DataSet stepData = dataReadResult.getDataSet(DataType.TYPE_STEP_COUNT_DELTA);

            // int totalSteps = 0;

//            for (DataPoint dp : stepData.getDataPoints()) {
//                for(Field field : dp.getDataType().getFields()) {
//                    int steps = dp.getValue(field).asInt();
//
//                    totalSteps += steps;
//
//                }
//            }
//
//            Log.d("Total", "getStepsToday" + totalSteps);
//            Val1= Integer.toString(totalSteps);


            DataSourcesRequest dataSourceRequest = new DataSourcesRequest.Builder()
                    // .setDataTypes( DataType.TYPE_STEP_COUNT_CUMULATIVE )
                    .setDataTypes(DataType.TYPE_STEP_COUNT_CUMULATIVE)
                    .setDataSourceTypes(DataSource.TYPE_RAW)
                    .build();

            final DataReadRequest readRequest = new DataReadRequest.Builder()
                    .read(DataType.TYPE_STEP_COUNT_DELTA)
                    .setTimeRange(startTime, endTime, TimeUnit.MILLISECONDS)
                    .build();

            ResultCallback<DataSourcesResult> dataSourcesResultCallback = new ResultCallback<DataSourcesResult>() {
                @Override
                public void onResult(DataSourcesResult dataSourcesResult) {
                    for (DataSource dataSource : dataSourcesResult.getDataSources()) {
//                        if( DataType.TYPE_STEP_COUNT_CUMULATIVE.equals( dataSource.getDataType() ) ) {
//                            registerFitnessDataListener(dataSource, DataType.TYPE_STEP_COUNT_CUMULATIVE);
//                        }
                        if (DataType.TYPE_STEP_COUNT_CUMULATIVE.equals(dataSource.getDataType())) {
                            registerFitnessDataListener(dataSource, DataType.TYPE_STEP_COUNT_CUMULATIVE);
                        }
                    }
                }
            };

            Fitness.SensorsApi.findDataSources(mGoogleApiClient, dataSourceRequest)
                    .setResultCallback(dataSourcesResultCallback);

            Fitness.RecordingApi.subscribe(mGoogleApiClient, DataType.TYPE_STEP_COUNT_CUMULATIVE)
                    .setResultCallback(mSubscribeResultCallback);

            //getStepsToday();

        } catch (Exception e) {
            Log.e("Error", "stepsss" + e.getMessage());
            e.printStackTrace();
        }


        signOutUI();


    }

    public void setFirstTime() {
        firstTime = 1;
    }

    public void startChallenge() {
        final FragmentManager sfm2 = getSupportFragmentManager();

        if (supportMapFragment.isAdded())
            sfm2.beginTransaction().hide(supportMapFragment).commit();

        if (!supportMapFragment.isAdded()) {
            sfm2.beginTransaction().add(R.id.map, supportMapFragment).commit();
            //  goToLocation(-27.476591, 153.028118);
        } else
            sfm2.beginTransaction().show(supportMapFragment).commit();

    }

    public void StartSensors() {
        extraStepCountok = true;
        DataSourcesRequest dataSourceRequest = new DataSourcesRequest.Builder()
                // .setDataTypes( DataType.TYPE_STEP_COUNT_CUMULATIVE )
                .setDataTypes(DataType.TYPE_STEP_COUNT_CUMULATIVE)
                .setDataSourceTypes(DataSource.TYPE_RAW)
                .build();
        ResultCallback<DataSourcesResult> dataSourcesResultCallback = new ResultCallback<DataSourcesResult>() {
            @Override
            public void onResult(DataSourcesResult dataSourcesResult) {
                for (DataSource dataSource : dataSourcesResult.getDataSources()) {
                    if (DataType.TYPE_STEP_COUNT_CUMULATIVE.equals(dataSource.getDataType())) {
                        registerFitnessDataListener(dataSource, DataType.TYPE_STEP_COUNT_CUMULATIVE);
                    }
                }
            }
        };
        Fitness.SensorsApi.findDataSources(mGoogleApiClient, dataSourceRequest)
                .setResultCallback(dataSourcesResultCallback);

        Fitness.RecordingApi.subscribe(mGoogleApiClient, DataType.TYPE_STEP_COUNT_CUMULATIVE)
                .setResultCallback(mSubscribeResultCallback);


    }

    private void registerFitnessDataListener(DataSource dataSource, DataType dataType) {

        SensorRequest request = new SensorRequest.Builder()
                .setDataSource(dataSource)
                .setDataType(dataType)
                .setSamplingRate(3, TimeUnit.SECONDS)
                .build();

        Fitness.SensorsApi.add(mGoogleApiClient, request, this)
                .setResultCallback(new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        if (status.isSuccess()) {
                            Log.e("GoogleFit", "SensorApi successfully added");
                        }
                    }
                });
    }

    @Override
    public void onDataPoint(DataPoint dataPoint) {

        for (final Field field : dataPoint.getDataType().getFields()) {
            final Value value = dataPoint.getValue(field);

            AsyncTask.execute(new Runnable() {
                @Override
                public void run() {

                    totalStepCount++;
                    if (extraStepCountok == true) {
                        extraStepCount++;
                    }
                    Log.d("stepdata", "Field: " + field.getName() + " Total Value: " + value);
                    Log.d("stepdata", "extraStepCount: " + extraStepCount);
                }
            });
//
//            runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
////                    if(setInternet==-1)
////                    {
////                          Toast.makeText(MainActivity.this, "No internet", Toast.LENGTH_SHORT).show();
////                    }
////                    else {
//                    totalStepCount++;
//
//                    if (extraStepCountok == true) {
//
//
//                        extraStepCount++;
//
//
//                    }
//                    //  }
//                    Log.d("stepdata", "Field: " + field.getName() + " Total Value: " + value);
//                    Log.d("stepdata", "extraStepCount: " + extraStepCount);
//
//                    //Val1 = Integer.toString(totalStepCount);
//                  //  Val1 = value.toString();
//                    //  Toast.makeText(MainActivity.this, "Field: " + field.getName() + " Value: " + value, Toast.LENGTH_SHORT).show();
//
//                }
//            });

            displayStepDataForToday();
            stepCount = value.asInt();

        }
    }


    private void handleNewLocation(Location location) {


        double currentLatitude = location.getLatitude();
        double currentLongitude = location.getLongitude();
        LatLng latLng = new LatLng(currentLatitude, currentLongitude);

//        MarkerOptions options = new MarkerOptions()
//                .position(latLng)
//                .title("I am here!");
//        mMap.addMarker(options);
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
    }


    public void checkForChallenges() {
        try {
            new ChallengesTask(this).execute(personName.toString());

        } catch (Exception e) {

        }

        try {

            Log.d("details for:", "details for:" + personName.toString() + today);
            new ChallengeCountNotifyTask(this).execute(personName.toString(), today);
        } catch (Exception ez) {

        }

    }

//    public String getNearbyToofferChallenge(String mygeofence) {
//        LatLng nearBy = null;
//        destinationBlock = null;
//        Log.d("came here1", "to get nearby " + mygeofence);
//        // int i=0;
//        for (int i = 0; i < ALLBLOCKLOCATIONS.length; i++) {
//            Log.d("came here4 ", ALLBLOCKLOCATIONS[i].getmId());
//            if (mygeofence.trim().equalsIgnoreCase(ALLBLOCKLOCATIONS[i].getmId())) {
//                nearBy = ALLBLOCKLOCATIONS[i].getMnearby()[0];
//                destinationBlock = ALLBLOCKLOCATIONS[i];
//                Log.d("desitnation", "became not null");
//            }
//        }
//
//        // Toast.makeText(getBaseContext(), "my Near by =>" + myBlockLocation.getMnearby()[0] + " or " + myBlockLocation.getMnearby()[1], Toast.LENGTH_LONG).show();
//
//        return getIDfromLatLng(nearBy);
//        //sned the notification mentioning you ahve a challeneg. And then forward the player to this location if accepted..
//        // Log.d("testing",mygeofence);
//
//
//    }

//    public String getIDfromLatLng(LatLng latLng) {
//
//        Log.d("MyGeonearby 1 ", "came here2");
//        //   Log.d("MyGeonearby latitude ", (String.valueOf(latLng.latitude)));
//        // Log.d("MyGeonearby longtitude ", (String.valueOf(latLng.longitude)));
//        String ID = "aaa";
//        for (int i = 0; i < ALLBLOCKLOCATIONS.length; i++) {
//
//            // ID=ALLBLOCKLOCATIONS[i].getmId();
////          //  Log.d("MyGeonearby IDSss ", ID);
//            if (latLng.equals(ALLBLOCKLOCATIONS[i].getmLatLng())) {
//                ID = ALLBLOCKLOCATIONS[i].getmId();
//                Log.d("MyGeonearby 1 ", "came here3");
//            }
//
//        }
//
//        return ID;
//
//    }


    public boolean checkStatus(String username) throws ExecutionException, InterruptedException {
        loginTask = new LoginTask(this, 1);
        loginTask.delegate = this;


        String status = loginTask.execute(username).get();
//        Log.d("Status-UserGIven", username);
//        Log.d("Status-UserTaken", status);

        if (status.equalsIgnoreCase(username)) {
            return true;
        } else
            return false;


    }

    @Override
    public void processFinish(String output) {

        //   Log.d("UserName", output);
        logedIn = true;
    }

    private void signOutUI() {
        signInButton.setVisibility(View.GONE);
        blankScreen.setVisibility(View.GONE);
//        tvNotSignedIn.setVisibility(View.GONE);
//        signOutButton.setVisibility(View.VISIBLE);

        if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {
            Person currentPerson = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
            String userid = currentPerson.getDisplayName(); //BY THIS CODE YOU CAN GET CURRENT LOGIN USER ID
            //   Toast.makeText(MainActivity.this, userid, Toast.LENGTH_SHORT).show();
        }


        //    viewContainer.setVisibility(View.VISIBLE);
    }

    private void signInUI() {
        blankScreen.setVisibility(View.GONE);
        signInButton.setVisibility(View.VISIBLE);
        // tvNotSignedIn.setVisibility(View.VISIBLE);
        // signOutButton.setVisibility(View.GONE);
        //   showCurrentLocation();
        //  viewContainer.setVisibility(View.GONE);
    }

    private void getProfileInformation() {

    }

    @Override
    public void onConnectionSuspended(int i) {
        signInUI();
        Log.e("HistoryAPI", "onConnectionSuspended");

    }

    private void onSignInClicked() {
        if (!mGoogleApiClient.isConnecting()) {
            mShouldResolve = true;
            resolveSignInError();
        }
    }

    private void onSignOutClicked() {
        if (mGoogleApiClient.isConnected()) {
            Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
            mGoogleApiClient.disconnect();
            signInUI();
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {

        //   Log.d("tag", "result is " + result);
        if (!result.hasResolution()) {
            GooglePlayServicesUtil.getErrorDialog(result.getErrorCode(), this,
                    0).show();
            return;
        }


        if (!mIntentInProgress) {

            connectionResult = result;

            if (mShouldResolve) {

                resolveSignInError();
            }
        }

        if (!authInProgress) {
            try {
                authInProgress = true;
                connectionResult.startResolutionForResult(MainActivity.this, REQUEST_OAUTH);
            } catch (IntentSender.SendIntentException e) {

            }
        } else {
            Log.e("GoogleFit", "authInProgress");
        }

        Log.e("HistoryAPI", "onConnectionFailed");
    }

    @Override
    public void onStart() {
        super.onStart();

        client.connect();
        mGoogleApiClient.connect();


    }

    @Override
    public void onStop() {
        super.onStop();
        Date dte = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");

        //getStepsToday();
        //   Log.d("stopiing", "onStop");
        // displayStepDataForToday();
        if (extraStepCount > 0) {

            // addStepDataToGoogleFit();


            editor.putInt("extrastep", extraStepCount);
            editor.putString("today", sdf22.format(dte));
            editor.apply();

            Log.d("stoping", "onStop" + Val1);


            new AddStepDataTask().execute(Integer.toString(ID), Val1, sdf.format(dte), Integer.toString(extraStepCount));
        }
        if (mGoogleApiClient.isConnected()) {
            // mGoogleApiClient.disconnect();
        }
        // client.disconnect();
        // mClient.disconnect();
    }


    private void resolveSignInError() {

        try {
            if (connectionResult.hasResolution()) {
                try {
                    mIntentInProgress = true;
                    connectionResult.startResolutionForResult(this, RC_SIGN_IN);
                } catch (IntentSender.SendIntentException e) {
                    mIntentInProgress = false;
                    mGoogleApiClient.connect();
                }
            }
        } catch (Exception ex) {

        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        //   Log.d("TAG", "Result=" + ConnectionResult.SUCCESS);
        if (GooglePlayServicesUtil.isGooglePlayServicesAvailable(this) == ConnectionResult.SUCCESS) {
//            Log.d("TAG", "test-1");
//            Log.d("API", "connected");
//
//            Log.d("TAG", "test-2");
        } else {
            //  Log.d("TAG", "test-3=");
            GooglePlayServicesUtil.getErrorDialog(
                    GooglePlayServicesUtil.isGooglePlayServicesAvailable(this),
                    this, 0);
        }


    }


    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        Log.i(TAG, "onRequestPermissionResult");
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length <= 0) {
                // If user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
                Log.i(TAG, "User interaction was cancelled.");
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission was granted.
//                buildFitnessClient();
            } else {
                // Permission denied.

                // In this Activity we've chosen to notify the user that they
                // have rejected a core permission for the app since it makes the Activity useless.
                // We're communicating this message in a Snackbar since this is a sample app, but
                // core permissions would typically be best requested during a welcome-screen flow.

                // Additionally, it is important to remember that a permission might have been
                // rejected without asking the user for permission (device policy or "Never ask
                // again" prompts). Therefore, a user interface affordance is typically implemented
                // when permissions are denied. Otherwise, your app could appear unresponsive to
                // touches or interactions which have required permissions.
                Snackbar.make(
                        findViewById(R.id.main_activity_view),
                        R.string.permission_denied_explanation,
                        Snackbar.LENGTH_INDEFINITE)
                        .setAction(R.string.settings, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                // Build intent that displays the App settings screen.
                                Intent intent = new Intent();
                                intent.setAction(
                                        Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                Uri uri = Uri.fromParts("package",
                                        BuildConfig.APPLICATION_ID, null);
                                intent.setData(uri);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        })
                        .show();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            if (resultCode != RESULT_OK) {
                mShouldResolve = false;
            }

            mIntentInProgress = false;

            if (!mGoogleApiClient.isConnecting() && !mGoogleApiClient.isConnected()) {
                mGoogleApiClient.connect();
            }


        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d("TAG", "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            //    mStatusTextView.setText(getString(R.string.signed_in_fmt, acct.getDisplayName()));
            updateUI(true);
        } else {
            // Signed out, show unauthenticated UI.
            updateUI(false);
        }
    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void signOut() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        // [START_EXCLUDE]
                        updateUI(false);
                        // [END_EXCLUDE]
                    }
                });
    }

    private void revokeAccess() {
        Auth.GoogleSignInApi.revokeAccess(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        // [START_EXCLUDE]
                        updateUI(false);
                        // [END_EXCLUDE]
                    }
                });
    }

    private void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();
    }

    private void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.hide();
        }
    }


    private void updateUI(boolean signedIn) {
        if (signedIn) {


            if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {
                Person currentPerson = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
                String userid = currentPerson.getId(); //BY THIS CODE YOU CAN GET CURRENT LOGIN USER ID
                //  Toast.makeText(MainActivity.this, userid, Toast.LENGTH_SHORT).show();
            }


            findViewById(R.id.sign_in_button).setVisibility(View.GONE);

//            Pattern emailPattern = Patterns.EMAIL_ADDRESS; // API level 8+
//            Account[] accounts = AccountManager.get(this).getAccountsByTypeForPackage("com.google","com.example.hsport.qutnettracker");//.getAccountsByType();
//            Account account=accounts[0];
//            String possibleEmail = account.name;
//            Toast.makeText(MainActivity.this, possibleEmail, Toast.LENGTH_SHORT).show();


//            for (Account account : accounts) {
//                Log.d("TAG", "came here");
//                if (emailPattern.matcher(account.name).matches()) {
//                    Log.d("TAG", "came here 2");
//                    String possibleEmail = account.name;
//                   // Toast.makeText(MainActivity.this, possibleEmail, Toast.LENGTH_SHORT).show();
//                }
//            }


            //    findViewById(R.id.sign_out_and_disconnect).setVisibility(View.VISIBLE);
        } else {
            //  mStatusTextView.setText(R.string.signed_out);

            findViewById(R.id.sign_in_button).setVisibility(View.VISIBLE);
//            findViewById(R.id.sign_out_and_disconnect).setVisibility(View.GONE);
        }
    }

    private GeofencingRequest getGeofencingRequest() {
        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();
        builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER);
        builder.addGeofences(mGeofences);
        return builder.build();
    }

    private PendingIntent getGeofencePendingIntent() {
        // Reuse the PendingIntent if we already have it.
        if (mPendingIntent != null) {
            return mPendingIntent;
        }


        Intent intent = new Intent(this, GeofenceTransitionsIntentService.class);
        // We use FLAG_UPDATE_CURRENT so that we get the same pending intent back when
        // calling addGeofences() and removeGeofences().
        return PendingIntent.getService(this, 0, intent, PendingIntent.
                FLAG_UPDATE_CURRENT);
    }


    @Override
    public void onLocationChanged(Location location) {
        //  Log.d("Location", "changed");
        handleNewLocation(location);
        //showCurrentLocation(location);
    }

    public BlockLocation getDestinationBlock() {
//        destinationBlock = nearGeoBlock;
//        return destinationBlock;
        return nearGeoBlock;
    }

    public String getCurrentGeoID() {
        return curGeoID;
    }

    public void setCurGeoID(String Cur) {
        curGeoID = Cur;
        // Log.d("current", "current " + curGeoID);
    }

    public BlockLocation getCurGeoBlock(String ID) {
        BlockLocation curentBlock = null;
        for (int i = 0; i < ALLBLOCKLOCATIONS.length; i++) {
            if (ALLBLOCKLOCATIONS[i].getmId().equals(ID))
                curentBlock = ALLBLOCKLOCATIONS[i];
        }
        return curentBlock;

    }

    public BlockLocation getNearByBlock() {
        return nearGeoBlock;
    }

    public void setNearByBlock(String id) {

        Log.d("detination", "destinationBlock" + 1);
        nearGeoBlock = getCurGeoBlock(id);
        destinationBlock = getCurGeoBlock(id);

        Log.d("detination", "destinationBlock" + destinationBlock.getmId());
    }

    public String getNearByID() {

        destinationBlock = nearGeoBlock;
        return nearGeoBlock.getmId();

    }


    //In use, call this every 30 seconds in active mode, 60 in ambient on watch faces
    public void displayStepDataForToday() {
        DailyTotalResult result = Fitness.HistoryApi.readDailyTotal(mGoogleApiClient, DataType.TYPE_STEP_COUNT_DELTA).await(1, TimeUnit.MINUTES);

        showDataSet(result.getTotal());
        //  showDataForToday();
    }

    public void displayLastWeeksData() {
        Calendar cal = Calendar.getInstance();
        Date now = new Date();
        cal.setTime(now);
        long endTime = cal.getTimeInMillis();
        cal.add(Calendar.WEEK_OF_YEAR, -1);
        long startTime = cal.getTimeInMillis();

        java.text.DateFormat dateFormat = DateFormat.getDateInstance();
        Log.e("History", "Range Start: " + dateFormat.format(startTime));
        Log.e("History", "Range End: " + dateFormat.format(endTime));

        //Check how many steps were walked and recorded in the last 7 days
        DataReadRequest readRequest = new DataReadRequest.Builder()
                .aggregate(DataType.TYPE_STEP_COUNT_DELTA, DataType.AGGREGATE_STEP_COUNT_DELTA)
                .bucketByTime(1, TimeUnit.DAYS)
                .setTimeRange(startTime, endTime, TimeUnit.MILLISECONDS)
                .build();

        DataReadResult dataReadResult = Fitness.HistoryApi.readData(mGoogleApiClient, readRequest).await(1, TimeUnit.MINUTES);

        ShowStepsFragement showStepsFragement = ShowStepsFragement.newInstance(dataReadResult);


        android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, showStepsFragement);
        fragmentTransaction.commit();
        //Used for aggregated data
//        if (dataReadResult.getBuckets().size() > 0) {
//            Log.e("History", "Number of buckets: " + dataReadResult.getBuckets().size());
//
//
//
//            for (Bucket bucket : dataReadResult.getBuckets()) {
//                List<DataSet> dataSets = bucket.getDataSets();
//
//
//
//
////                for (DataSet dataSet : dataSets) {
////                    showDataSet(dataSet);
////
////
////                }
//            }
//        }
//        //Used for non-aggregated data
//        else if (dataReadResult.getDataSets().size() > 0) {
//            Log.e("History", "Number of returned DataSets: " + dataReadResult.getDataSets().size());
//            for (DataSet dataSet : dataReadResult.getDataSets()) {
//                showDataSet(dataSet);
//            }
//        }
    }

    public void showDataSet(DataSet dataSet) {


//        data.add(0, map);
//        data.get(0).get("name");


        //    Log.d("iteration", "number" + 1);

        try {
            Log.e("History", "Data returned for Data type: " + dataSet.getDataType().getName());
            DateFormat dateFormat = DateFormat.getDateInstance();
            DateFormat timeFormat = DateFormat.getTimeInstance();
            String Val = "";
            int totSteps = 0;
            int i = 0;
            for (DataPoint dp : dataSet.getDataPoints()) {
                Log.e("History", "Data point:");
                Log.e("History", "\tType: " + dp.getDataType().getName());

                Log.e("History", "\tStart: " + dateFormat.format(dp.getStartTime(TimeUnit.MILLISECONDS)) + " " + timeFormat.format(dp.getStartTime(TimeUnit.MILLISECONDS)));
                map.put("Start", dateFormat.format(dp.getStartTime(TimeUnit.MILLISECONDS)));
                Log.e("History", "\tEnd: " + dateFormat.format(dp.getEndTime(TimeUnit.MILLISECONDS)) + " " + timeFormat.format(dp.getStartTime(TimeUnit.MILLISECONDS)));
                map.put("End", dateFormat.format(dp.getEndTime(TimeUnit.MILLISECONDS)));

                for (Field field : dp.getDataType().getFields()) {
                    Log.e("History", "\tField: " + field.getName() + " Value: " + dp.getValue(field));

                    Val = dp.getValue(field).toString();

                }
                map.put("Steps", Val);
                data.add(i, map);
                i++;
            }

            Val1 = Val;


//                    .setAction("Action", null).show();
        } catch (Exception ee) {

        }
//
//Log.d("Size of data ","size is "+data.size());


//
//        Snackbar.make(findViewById(R.id.main_activity_view), "Value " + totSteps, Snackbar.LENGTH_LONG)
//                .setAction("Action", null).show();

//        ShowStepsFragement showStepsFragement  = ShowStepsFragement.newInstance(data);
//
//
//
//        android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
//        fragmentTransaction.replace(R.id.fragment_container, showStepsFragement);
//        fragmentTransaction.commit();


    }

    public void showDataForToday() {
        Snackbar.make(findViewById(R.id.main_activity_view), "Todays Steps " + stepCount, Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
    }

    public void addStepDataToGoogleFit() {
        DateFormat dateFormat = DateFormat.getDateInstance();
        //Adds steps spread out evenly from start time to end time
        Calendar cal = Calendar.getInstance();
        Date now = new Date();
        cal.setTime(now);
        long endTime = cal.getTimeInMillis();
        cal.add(Calendar.HOUR_OF_DAY, -1);
        long startTime = cal.getTimeInMillis();

        Date dte = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        //  System.out.println();
        //   Log.d("Start", "Start Time" + startTime);
        // Log.d("Start", "End Time" + sdf.format(dte));

        DataSource dataSource = new DataSource.Builder()
                .setAppPackageName(this)
                .setDataType(DataType.TYPE_STEP_COUNT_DELTA)
                .setName("Step Count")
                .setType(DataSource.TYPE_RAW)
                .build();

        // int stepCountDelta = 1000000;
        int stepCountDelta = stepCount;
        DataSet dataSet = DataSet.create(dataSource);


        // int id=getUserID();
        DataPoint point = dataSet.createDataPoint()
                .setTimeInterval(startTime, endTime, TimeUnit.MILLISECONDS);
        point.getValue(Field.FIELD_STEPS).setInt(stepCountDelta);
        dataSet.add(point);


        Val1 = point.getValue(Field.FIELD_STEPS).toString();


        new AddStepDataTask().execute(Integer.toString(ID), Val1, sdf.format(dte), Integer.toString(extraStepCount));

        Status status = Fitness.HistoryApi.insertData(mGoogleApiClient, dataSet).await(1, TimeUnit.MINUTES);

        if (!status.isSuccess()) {
            Log.e("History", "Problem with inserting data: " + status.getStatusMessage());
        } else {
            Log.e("History", "data inserted");
        }
    }

    public void setUserID(int id) {
        ID = id;

        //This is the best place to get extra step count when connecting...
        Date dte = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        String userID = Integer.toString(ID);
        // String userID= "5";

        //   Log.d("IDD", "ID is " + ID);
        // Log.d("IDD", "ID is string " + userID);
        searchExtraStepsTask = (SearchExtraStepsTask) new SearchExtraStepsTask().execute(userID, sdf.format(dte).toString());
        searchTotalStepsTask = (SearchTotalStepsTask) new SearchTotalStepsTask().execute(userID, sdf.format(dte).toString());
    }

    public int getUserID() {
        return ID;
    }

    public String getUserName() {
        return personName;
    }

    public void updateStepDataOnGoogleFit() {
        //If two entries overlap, the new data is dropped when trying to insert. Instead, you need to use update
        Calendar cal = Calendar.getInstance();
        Date now = new Date();
        cal.setTime(now);
        long endTime = cal.getTimeInMillis();
        cal.add(Calendar.HOUR_OF_DAY, -1);
        long startTime = cal.getTimeInMillis();

        DataSource dataSource = new DataSource.Builder()
                .setAppPackageName(this)
                .setDataType(DataType.TYPE_STEP_COUNT_DELTA)
                .setName("Step Count")
                .setType(DataSource.TYPE_RAW)
                .build();

        //int stepCountDelta = 2000000;
        int stepCountDelta = stepCount;
        DataSet dataSet = DataSet.create(dataSource);

        DataPoint point = dataSet.createDataPoint()
                .setTimeInterval(startTime, endTime, TimeUnit.MILLISECONDS);
        point.getValue(Field.FIELD_STEPS).setInt(stepCountDelta);
        dataSet.add(point);

        DataUpdateRequest updateRequest = new DataUpdateRequest.Builder().setDataSet(dataSet).setTimeInterval(startTime, endTime, TimeUnit.MILLISECONDS).build();
        Fitness.HistoryApi.updateData(mGoogleApiClient, updateRequest).await(1, TimeUnit.MINUTES);
    }

    public void deleteStepDataOnGoogleFit() {
        Calendar cal = Calendar.getInstance();
        Date now = new Date();
        cal.setTime(now);
        long endTime = cal.getTimeInMillis();
        cal.add(Calendar.DAY_OF_YEAR, -1);
        long startTime = cal.getTimeInMillis();

        DataDeleteRequest request = new DataDeleteRequest.Builder()
                .setTimeInterval(startTime, endTime, TimeUnit.MILLISECONDS)
                .addDataType(DataType.TYPE_STEP_COUNT_DELTA)
                .build();

        Fitness.HistoryApi.deleteData(mGoogleApiClient, request).await(1, TimeUnit.MINUTES);
    }

    public void cancelSubscriptions() {
        Fitness.RecordingApi.unsubscribe(mGoogleApiClient, DataType.TYPE_STEP_COUNT_DELTA)
                .setResultCallback(mCancelSubscriptionResultCallback);

        Fitness.SensorsApi.remove(mGoogleApiClient, this)
                .setResultCallback(new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        if (status.isSuccess()) {
                            // mGoogleApiClient.disconnect();

                            extraStepCountok = false;
                            //     Log.d("Stopped", "Stopped getting extra steps");
                        }
                    }
                });
    }

    public void showSubscriptions() {
        Fitness.RecordingApi.listSubscriptions(mGoogleApiClient)
                .setResultCallback(mListSubscriptionsResultCallback);

//        Fitness.RecordingApi.subscribe(mGoogleApiClient, DataType.TYPE_STEP_COUNT_CUMULATIVE)
//                .setResultCallback(mSubscribeResultCallback);
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(AUTH_PENDING, authInProgress);
    }

    public void setExtraSteps(String ex) {
        try {
            if (!ex.equals("")) {
                extraStepCount = Integer.parseInt(ex);

            } else
                extraStepCount = -1;
            // setInternet=0;
        } catch (Exception e) {

            // setInternet=-1;
            // Toast.makeText(this,"no interebt",Toast.LENGTH_SHORT);
            //  Log.d("noint","no int");
            //  extraStepCount=-10;


        }

    }

    public void setTotSteps(String ex) {
        try {
            if (!ex.equals(""))
                totalStepCount = Integer.parseInt(ex);
            else
                totalStepCount = -1;
            // setInternet=0;
        } catch (Exception e) {

            // setInternet=-1;
            // Toast.makeText(this,"no interebt",Toast.LENGTH_SHORT);
            //  Log.d("noint","no int");
            //  extraStepCount=-10;


        }

    }


    public void resetDestination() {
        destinationBlock = null;
    }

    public void updateChallegeCount() {

        int uid = getUserID();
        Date dte = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        String ste = sdf.format(dte).toString();

        Log.e("calling", "calling");
        new updateChalllengeCountTask().execute(Integer.toString(uid), ste);
    }


    public void setChallengeInProgress(int i) {
        challengeInProgress = i;
    }

    public int getChallengeInProgress() {
        return challengeInProgress;
    }

    public void sendConfirmation() {
        showAleart();

    }

    public void showAleart() {
        try {
//        Intent intent = new Intent(this, DialogActivty.class);
////        EditText editText = (EditText) findViewById(R.id.edit_message);
////        String message = editText.getText().toString();
////        intent.putExtra(EXTRA_MESSAGE, message);
//        startActivity(intent);
        } catch (Exception e) {
        }

//        Log.d("Confirm", "confirming");
//        final AlertDialog.Builder builder1 = new AlertDialog.Builder(MainActivity.this);
//        builder1.setMessage("Congratulations !!! Challenge Completed")
//                .setCancelable(false)
//                .setIcon(R.drawable.trophy)
//                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                    public void onClick(final DialogInterface dialog, final int id) {
//                        Log.e("Activity", "Clicked on exit");
//                        // System.exit(0);
//                    }
//                });
//        Log.d("Confirm", "confirming2");
//        final AlertDialog alert = builder1.create();
//        Log.d("Confirm", "confirming3");
//        //alert.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
//        alert.show();
    }


    protected BroadcastReceiver mNotificationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            try {
                //     Log.d("came", "broadcast came");

                //   Log.d("Confirm", "confirming");
                final AlertDialog.Builder builder1 = new AlertDialog.Builder(MainActivity.this);
                builder1.setMessage("Congratulations !!! Challenge Completed")
                        .setCancelable(false)
                        .setIcon(R.drawable.trophy)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(final DialogInterface dialog, final int id) {
                                Log.e("Activity", "Clicked on exit");
                                // System.exit(0);
                            }
                        });
                //   Log.d("Confirm", "confirming2");
                final AlertDialog alert = builder1.create();
                // Log.d("Confirm", "confirming3");
                //alert.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
                alert.show();
            } catch (Exception e) {
            }


            // Snackbar.make(findViewById(R.id.main_activity_view), "Replace with your own action", Snackbar.LENGTH_LONG);
//                        .setAction("Action", null).show();
        }
    };


    public boolean getNotification() {
        return notificationSent;
    }


    public void settNotification() {
        notificationSent = false;
    }


    public void addCompletedChallenge(String Block) {

        Date dte = new Date();
        SimpleDateFormat sdf222 = new SimpleDateFormat("yyyy/MM/dd");

        Log.e(TAG, "calling completed 1");
        new AddCompletedChallngestask().execute(personName.toString(), Block, sdf222.format(dte));

    }


    private void getStepsToday() {

        // new getStepDataTask().execute();
//        Calendar cal = Calendar.getInstance();
//        Date now = new Date();
//        cal.setTime(now);
//        long endTime = cal.getTimeInMillis();
//        cal.add(Calendar.HOUR_OF_DAY, -1);
//        long startTime = cal.getTimeInMillis();


//        Calendar cal = Calendar.getInstance();
//        Date now = new Date();
//        cal.setTime(now);
//        long endTime = cal.getTimeInMillis();
//        cal.set(Calendar.HOUR_OF_DAY, 0);
//        cal.set(Calendar.MINUTE, 0);
//        cal.set(Calendar.SECOND, 0);
//        long startTime = cal.getTimeInMillis();
//
//        final DataReadRequest readRequest = new DataReadRequest.Builder()
//                .read(DataType.TYPE_STEP_COUNT_DELTA)
//                .setTimeRange(startTime, endTime, TimeUnit.MILLISECONDS)
//                .build();
//
//        DataReadResult dataReadResult =
//                Fitness.HistoryApi.readData(mGoogleApiClient, readRequest).await(1, TimeUnit.MINUTES);
//
//
//
//        DataSet stepData = dataReadResult.getDataSet(DataType.TYPE_STEP_COUNT_DELTA);
//
//        int totalSteps = 0;
//
//        for (DataPoint dp : stepData.getDataPoints()) {
//            for(Field field : dp.getDataType().getFields()) {
//                int steps = dp.getValue(field).asInt();
//
//                totalSteps += steps;
//
//            }
//        }
//
//        Log.d("Total","getStepsToday"+totalSteps);
//        Val1= Integer.toString(totalSteps);
//       // publishTodaysStepData(totalSteps);
    }

    public class getStepDataTask extends AsyncTask<Void, Void, Void> {


        @Override
        protected Void doInBackground(Void... params) {
            Calendar cal = Calendar.getInstance();
            Date now = new Date();
            cal.setTime(now);
            long endTime = cal.getTimeInMillis();
            cal.set(Calendar.HOUR_OF_DAY, 0);
            cal.set(Calendar.MINUTE, 0);
            cal.set(Calendar.SECOND, 0);
            long startTime = cal.getTimeInMillis();

            final DataReadRequest readRequest = new DataReadRequest.Builder()
                    .read(DataType.TYPE_STEP_COUNT_DELTA)
                    .setTimeRange(startTime, endTime, TimeUnit.MILLISECONDS)
                    .build();

            DataReadResult dataReadResult =
                    Fitness.HistoryApi.readData(mGoogleApiClient, readRequest).await(1, TimeUnit.MINUTES);


            DataSet stepData = dataReadResult.getDataSet(DataType.TYPE_STEP_COUNT_DELTA);

            int totalSteps = 0;

            for (DataPoint dp : stepData.getDataPoints()) {
                for (Field field : dp.getDataType().getFields()) {
                    int steps = dp.getValue(field).asInt();

                    totalSteps += steps;

                }
            }

            Log.d("Total", "getStepsToday" + totalSteps);
            Val1 = Integer.toString(totalSteps);
            return null;
        }
    }


    public void setNotificationCOunt(int count) {
        badgeCount = count;


    }
//    private void publishTodaysStepData(int totalSteps) {
//     //   Intent intent = new Intent(HISTORY_INTENT);
//        // You can also include some extra data.
//       // intent.putExtra(HISTORY_EXTRA_STEPS_TODAY, totalSteps);
//
//        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
//    }
}