package com.example.hsport.qutnettracker;


import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.example.hsport.qutnettracker.Tasks.SearchGroupIDtask;
import com.example.hsport.qutnettracker.Tasks.SearchGroupsTask;
import com.example.hsport.qutnettracker.Tasks.SearchUserIDTask;
import com.example.hsport.qutnettracker.Tasks.addGroupTask;
import com.google.android.gms.plus.model.people.Person;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class GroupFragment extends Fragment {

    private EditText groupName, groupType;
    private TextView gGroup, gType;
    public static List<String> list = new ArrayList<String>();
    static List<String> GroupNames = new ArrayList<String>();
    private SearchGroupsTask searchGroupsTask;
    private SearchUserIDTask searchUserIDTask;
    private SearchGroupIDtask searchGroupIDtask;

    public static View rootView;
    public static String memID;
    public static int ID;
    public static int GID;
    private addGroupTask AddGroupTask;

    public GroupFragment() {
        // Required empty public constructor
    }

    public static GroupFragment newInstance(String C) {
        GroupFragment fragment = new GroupFragment();
        Bundle args = new Bundle();

        memID = C;
        return fragment;
    }

    public static GroupFragment newInstance() {
        GroupFragment fragment = new GroupFragment();
        Bundle args = new Bundle();

        // Challenger = C;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        rootView = inflater.inflate(R.layout.fragment_group, container, false);

        groupName = (EditText) rootView.findViewById(R.id.txtGroupName);
        groupType = (EditText) rootView.findViewById(R.id.txtGroupType);

//        gGroup = (TextView)rootView.findViewById(R.id.textView6);
//        gType = (TextView)rootView.findViewById(R.id.textView7);


        Button button = (Button) rootView.findViewById(R.id.btnAddGroup);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addGroup(rootView);
                groupName.setText("");
                groupType.setText("");
                final AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
                builder1.setMessage("Group Successfully Added")
                        .setCancelable(false)
                        .setIcon(R.drawable.trophy)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(final DialogInterface dialog, final int id) {


                                GroupFragment groupFragment = GroupFragment.newInstance(memID);


                                android.support.v4.app.FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                                fragmentTransaction.replace(R.id.fragment_container, groupFragment);
                                fragmentTransaction.commit();

//                                Log.e("Activity", "Clicked on exit");
//                                Intent notificationIntent = new Intent(getActivity(), MainActivity.class);
//                                notificationIntent.putExtra("mapFragment", "OfferedChallenge");
//                                // Construct a task stack.
//                                TaskStackBuilder stackBuilder = TaskStackBuilder.create(getActivity());
//                                // Add the main Activity to the task stack as the parent.
//                                stackBuilder.addParentStack(MainActivity.class);
//                                // Push the content Intent onto the stack.
//                                stackBuilder.addNextIntent(notificationIntent);
//
//                                startActivity(notificationIntent);

                                // System.exit(0);
                            }
                        });
                final AlertDialog alert = builder1.create();
                alert.show();
                // do something
            }
        });


        searchGroupsTask = (SearchGroupsTask) new SearchGroupsTask(this).execute();


        return rootView;

    }

    public void addGroup(View v) {
        String gName = groupName.getText().toString();
        String gType = groupType.getText().toString();

        AddGroupTask = (addGroupTask) new addGroupTask(this).execute(gName, gType);
        // new addGroupTask(this).execute(gName,gType);

    }

    public void setList(List<String> list) {
        this.list = list;
        ArrayAdapter<String> ArAd = new ArrayAdapter<String>(getActivity(), R.layout.rowlayout, R.id.txtItem, list);

        //  ArrayAdapter<String> ArAd = new ArrayAdapter<String>(getActivity(),R.layout.rowlayout, R.id.txtItem,GroupNames);

        ListView lv = (ListView) rootView.findViewById(R.id.m_list);
        lv.setAdapter(ArAd);
        //  setListAdapter(ArAd);
        setRetainInstance(true);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ViewGroup rootView = (ViewGroup) view;
                TextView groupName = (TextView) rootView.findViewById(R.id.txtItem);


            //    Log.d("clicked", (String) groupName.getText());

                //   Toast.makeText(getActivity(), textView.getText(), Toast.LENGTH_SHORT).show();
                //  new SearchUserIDTask().execute(groupName.getText().toString());
                searchUserIDTask = (SearchUserIDTask) new SearchUserIDTask(GroupFragment.this).execute(memID);
                searchGroupIDtask = (SearchGroupIDtask) new SearchGroupIDtask(GroupFragment.this).execute(groupName.getText().toString().trim());
                // searchUserIDTask=  new SearchUserIDTask(this).execute(memID);


                new InsertToGroup().execute(ID, GID);

            }
        });

    }

    public void setUserID(int id) {
        ID = id;
    }

    public void setGroupID(int id) {
        GID = id;
    }


    public class InsertToGroup extends AsyncTask<Integer, Integer, Void> {
        public JSONObject json;
        private static final String TAG_SUCCESS = "code";
        private Context context;
        private Person person;

      //  MainActivity mainActivity = new MainActivity();

        @Override
        protected Void doInBackground(Integer... params) {

            try {
                ContentValues contentValues = new ContentValues();
                Integer UID = (Integer) params[0];
                Integer GroupID = (Integer) params[1];
                // String urlToPhoto = "URL";
                String Status = "current";


                List<NameValuePair> pair = new ArrayList<>();
                pair.add(new BasicNameValuePair("UID", UID.toString()));
                pair.add(new BasicNameValuePair("GroupID", GroupID.toString()));
                pair.add(new BasicNameValuePair("Status", Status));
                //   pair.add(new BasicNameValuePair("status",String,psswd);


                json = JSONParser.makeHttpRequest("http://sharethat.us/QUT/addTogroup.php", "GET", pair);

            //    Log.d("Create Response", json.toString());
              //  Log.d("Create Response", json.getString("data"));
                if(json.getString("data").equals("[[\"Record not Inserted\"]]"))
                {


                    Snackbar.make(rootView, "You are already a member of this group", Snackbar.LENGTH_LONG)
                            .setAction("Ok", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {

                                }
                            })
                            .setActionTextColor(Color.RED).show();
                }
                else
                    Snackbar.make(rootView, "Successfully joined the group", Snackbar.LENGTH_LONG)
                            .setAction("Ok", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {

                                }
                            })
                            .setActionTextColor(Color.GREEN).show();




            } catch (Exception e) {



              //  Log.d("Exception Insert", e.getMessage());

            }
            return null;

        }


    }


}
