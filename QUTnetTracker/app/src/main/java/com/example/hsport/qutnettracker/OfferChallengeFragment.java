package com.example.hsport.qutnettracker;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.hsport.qutnettracker.Tasks.AddChallengetask;
import com.example.hsport.qutnettracker.Tasks.FriendsSearchTask;
import com.google.android.gms.plus.model.people.PersonBuffer;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class OfferChallengeFragment extends ListFragment  implements AdapterView.OnItemSelectedListener {

    //public GoogleApiClient mGoogleApiClient;
    public static PersonBuffer personBuffer;
    ListView lv;
    public static List<String> list = new ArrayList<String>();
    static List<String> Names = new ArrayList<String>();
    public static String Challenger;
    public static String challengee;
    private AddChallengetask addChallengetask;
    public static Spinner spinner1;
    public static Spinner spinner2;
    private  String challenger;
    private static String chellengee;
    private static String block;
    private static String message;
    public static View rootView;
    public EditText editText;


    public JSONObject json ;
    private FriendsSearchTask friendsSearchTask;

    public OfferChallengeFragment() {
        // Required empty public constructor


    }


//    public static OfferChallengeFragment newInstance(PersonBuffer persons) {
//        OfferChallengeFragment fragment = new OfferChallengeFragment();
//        Bundle args = new Bundle();
//
//        int count = persons.getCount();
//        String ID;
//        String Name;
//
//        personBuffer = persons;
//
//        // showList(personBuffer);
//
//        for (int i = 0; i < count; i++) {
//            Names.add(i, personBuffer.get(i).getDisplayName());
//            // Log.d("TAG", "Person " + i + " name: " + persons.get(i).getDisplayName() + " - id: " + persons.get(i).getId());
//
//
//        }
//        return fragment;
//    }

    public static OfferChallengeFragment newInstance(String C) {
        OfferChallengeFragment fragment = new OfferChallengeFragment();
        Bundle args = new Bundle();

        Challenger = C;
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
     //   Log.d("Came", "came");
        json=new JSONObject();

        Toast.makeText(getActivity(), Challenger, Toast.LENGTH_SHORT).show();

        rootView = (ViewGroup) inflater.inflate(R.layout.fragment_offer_challenge, container, false);

        /**
         * Inititlize.
         */
        //  lv = (ListView) rootView.findViewById(R.id.listView);

      // int count = personBuffer.getCount();
        // personBuffer

//        List<String> lst = new ArrayList<String>();
//        for (int i = 0; i < count; i++) {
//            lst.add(i, personBuffer.get(i).getDisplayName());
//           // Log.d("TAG", "Person " + i + " name: " + personBuffer.get(i).getDisplayName() + " - id: " + personBuffer.get(i).getId());
//
//
//
//            //  friendsList.add(i,persons);
//
//        }


//        List<String> lst = new ArrayList<String>();
//        for(int i = 1; i <= 10; i++) {
//            lst.add(i + "");
//        }

        //uncomment this

        friendsSearchTask = (FriendsSearchTask) new FriendsSearchTask(this).execute(Challenger);



       spinner2 = (Spinner) rootView.findViewById(R.id.blocks_spinner);
// Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.planets_array, android.R.layout.simple_spinner_item);
// Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
// Apply the adapter to the spinner
        spinner2.setAdapter(adapter);

        spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
              //  Log.d("Value", "Selected: ");
               block="0";
                try{
                    Object item = parent.getItemAtPosition(position);
                    block=item.toString();
                    //  curID = spinner.getSelectedItem().toString();
                //    Log.d("Value", "Selected block: "+block);
                    // mainActivity.setCurGeoID(curID);
                }
                catch (Exception E)
                {

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



        Button button = (Button)rootView.findViewById(R.id.btnSend);
        editText   = (EditText)rootView.findViewById(R.id.messege);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // loginPost(rootView);

             //   Log.e("clicked", "button Clicked");
             addChallenge(Challenger, challengee, block, editText.getText().toString());
                editText.setText("");
                spinner1.setSelection(0);
                spinner2.setSelection(0);
                final AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
                builder1.setMessage("Challenge Successfully Sent")
                        .setCancelable(false)
                        .setIcon(R.drawable.trophy)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(final DialogInterface dialog, final int id) {
                                Log.e("Activity", "Clicked on exit");
                                Intent notificationIntent = new Intent(getActivity(), MainActivity.class);
                                notificationIntent.putExtra("mapFragment", "OfferedChallenge");
                                // Construct a task stack.
                                TaskStackBuilder stackBuilder = TaskStackBuilder.create(getActivity());
                                // Add the main Activity to the task stack as the parent.
                                stackBuilder.addParentStack(MainActivity.class);
                                // Push the content Intent onto the stack.
                                stackBuilder.addNextIntent(notificationIntent);

                                startActivity(notificationIntent);

                                // System.exit(0);
                            }
                        });
                final AlertDialog alert = builder1.create();
                alert.show();
                // do something
            }
        });

        return rootView;
    }

    public void setList(List<String> list) {
        this.list = list;

        spinner1 = (Spinner) rootView.findViewById(R.id.Friends_spinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, list);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner1.setAdapter(adapter);



        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
              //  Log.d("Value", "Selected: ");
                challengee="0";
                try{
                    Object item = parent.getItemAtPosition(position);
                    challengee=item.toString();
                    //  curID = spinner.getSelectedItem().toString();
               //     Log.d("Value", "Selected friend: "+challengee);
                    // mainActivity.setCurGeoID(curID);
                }
                catch (Exception E)
                {

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

//    @Override
//    public void onListItemClick(ListView l, View view, int position, long id) {
//        super.onListItemClick(l, view, position, id);
//
//        ViewGroup rootView = (ViewGroup) view;
//        TextView textView = (TextView) rootView.findViewById(R.id.txtItem);
//
//        Toast.makeText(getActivity(), textView.getText(), Toast.LENGTH_SHORT).show();
//        new AddChallengetask().execute(Challenger, textView.getText().toString());
//
//    }

    public void addChallenge(String Challenger,String Challengee,String block, String mess)
    {
        Date dte = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");

        new AddChallengetask().execute(Challenger,Challengee,block,mess,sdf.format(dte));
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


//        List<String> lst = new ArrayList<String>();
//        for(int i = 1; i <= 10; i++) {
//            lst.add(i + "");
//        }
//
//        ArrayAdapter<String> ArAd = new ArrayAdapter<String>(getActivity(),
//                android.R.layout.simple_list_item_1, lst);
//        lv.setAdapter(ArAd);
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {




    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
