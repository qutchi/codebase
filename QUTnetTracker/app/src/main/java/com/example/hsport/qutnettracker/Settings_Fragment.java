package com.example.hsport.qutnettracker;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;


/**
 * A simple {@link Fragment} subclass.
 */
public class Settings_Fragment extends Fragment implements AdapterView.OnItemSelectedListener {

    public MainActivity mainActivity = new MainActivity();
    public Spinner spinner;
    public static int firstTime=0;

    public Settings_Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

      //  Log.d("came heare", "settings");

        final View rootView = inflater.inflate(R.layout.fragment_settings_, container, false);

        Spinner spinner = (Spinner) rootView.findViewById(R.id.planets_spinner);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.planets_array, android.R.layout.simple_spinner_item);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(this);


        return rootView;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        // An item was selected. You can retrieve the selected item using
        // parent.getItemAtPosition(pos)
      //  Log.d("Value", "Selected: ");
        String curID = "0";
        try {
            Object item = parent.getItemAtPosition(position);
            curID = item.toString();
            //  curID = spinner.getSelectedItem().toString();
       //     Log.d("Value", "Selected: " + curID);
            if(curID.equals("Please Select a Block"))
            {
                firstTime=1;
               // curID="S Block";
                mainActivity.setCurGeoID(curID);
            }else{
                mainActivity.setCurGeoID(curID);
                sendToMain();
            }








            //sendNotification();
        } catch (Exception E) {

        }
        // Spinner mySpinner=(Spinner) findViewById(R.id.your_spinner);


    }

    public void sendToMain() {


            final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage("Please confirm your location ?")
                    .setCancelable(false)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(final DialogInterface dialog, final int id) {
                            Intent intent = new Intent(getActivity(), MainActivity.class);

                            intent.putExtra("VALUE", "selected");
                            Log.e("Activity", "Activity is starting 1");
                            startActivity(intent);
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(final DialogInterface dialog, final int id) {
                            dialog.cancel();
                        }
                    });
            final AlertDialog alert = builder.create();
            alert.show();

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


}
