package com.example.hsport.qutnettracker;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by hkthilina on 5/12/2016.
 */
public class NotificationBroadcastReceiver extends BroadcastReceiver {
    MainActivity mainActivity = new MainActivity();
    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if(action.equals("notification_cancelled"))
        {
            String block=intent.getStringExtra("BlockValue");
            mainActivity.setN();
            mainActivity.setNearByBlockwhenCanceled(block);
            // your code
        }
    }
}
