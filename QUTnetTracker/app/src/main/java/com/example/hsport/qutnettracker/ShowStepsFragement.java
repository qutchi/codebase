package com.example.hsport.qutnettracker;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.android.gms.fitness.data.Bucket;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.result.DataReadResult;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;


/**
 * A simple {@link Fragment} subclass.
 */
public class ShowStepsFragement extends Fragment {


    private static Map<String, String> map = new HashMap<String, String>();
    // map.put("name", "demo");


    public static List<Map<String, String>> data = new ArrayList<>();
    public static List<DataSet> data1 = new ArrayList<>();
    public static View rootView;
    public static DataReadResult dataReadResult;
    //ListView lv;
    public static List<String> steps = new ArrayList<String>();

    public ShowStepsFragement() {
        // Required empty public constructor
    }

//    public static ShowStepsFragement newInstance(List<Map<String, String>> C ) {
//        ShowStepsFragement fragment = new ShowStepsFragement();
//        Bundle args = new Bundle();
//
//        data = C;
//        return fragment;
//    }


    public static ShowStepsFragement newInstance(DataReadResult C) {
        ShowStepsFragement fragment = new ShowStepsFragement();
        Bundle args = new Bundle();

        dataReadResult = C;
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_show_steps_fragement, container, false);


      //  Log.d("came", "came here" + dataReadResult.getBuckets().size());


        if (dataReadResult.getBuckets().size() > 0) {
            for (Bucket bucket : dataReadResult.getBuckets()) {
                List<DataSet> dataSets = bucket.getDataSets();
int j=0;
                for (DataSet dataSet : dataSets) {
                    // showDataSet(dataSet);

              //      Log.d("iteration", "number" + 1);


                    Log.e("History", "Data returned for Data type: " + dataSet.getDataType().getName());
                    DateFormat dateFormat = DateFormat.getDateInstance();
                    DateFormat timeFormat = DateFormat.getTimeInstance();
                    String Val = "";
                    int totSteps = 0;
                    int i = 0;
                    for (DataPoint dp : dataSet.getDataPoints()) {
                        Log.e("History", "Data point:");
                        Log.e("History", "\tType: " + dp.getDataType().getName());

                        Log.e("History", "\tStart: " + dateFormat.format(dp.getStartTime(TimeUnit.MILLISECONDS)) + " " + timeFormat.format(dp.getStartTime(TimeUnit.MILLISECONDS)));
                        map.put("Start", dateFormat.format(dp.getStartTime(TimeUnit.MILLISECONDS)));
                        Log.e("History", "\tEnd: " + dateFormat.format(dp.getEndTime(TimeUnit.MILLISECONDS)) + " " + timeFormat.format(dp.getStartTime(TimeUnit.MILLISECONDS)));
                        map.put("End", dateFormat.format(dp.getEndTime(TimeUnit.MILLISECONDS)));

                        for (Field field : dp.getDataType().getFields()) {
                            Log.e("History", "\tField: " + field.getName() + " Value: " + dp.getValue(field));

                            Val = dp.getValue(field).toString();

                        }
                        map.put("Steps", Val);
                        data.add(i, map);
                        i++;
                    }

                   steps.add(j,"Start: "+ map.get("Start")+" End: "+map.get("End")+" Steps: "+map.get("Steps"));


                }
            }
        }


//        for(int j=0;j<data.size();j++)
//            steps.add("start: "+data.get(j).get("Start")+" End: "+data.get(j).get("End")+" Step Count: "+data.get(j).get("Steps"));

        ListView lv = (ListView) rootView.findViewById(R.id.steps_list);
        //List<String> steps = new ArrayList<String>();
//        for (String row : steps) {
//            Log.d("start Date: ", row.toString());
//        }
//
//        for(int j=0;j<data.size();j++){
//            Log.d("start Date: ", data.get(j).get("Start"));
//            Log.d("end Date: ", data.get(j).get("End"));
//            Log.d("step count: ", data.get(j).get("Steps"));
//        }
//     //


        ArrayAdapter<String> ArAd = new ArrayAdapter<String>(getActivity(), R.layout.rowlayout, R.id.txtItem, steps);


        lv.setAdapter(ArAd);
        //  setListAdapter(ArAd);
        setRetainInstance(true);
        return rootView;
    }


}
