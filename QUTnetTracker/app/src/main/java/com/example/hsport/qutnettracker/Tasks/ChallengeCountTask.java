package com.example.hsport.qutnettracker.Tasks;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.example.hsport.qutnettracker.MainActivity;
import com.example.hsport.qutnettracker.NewsFragment;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

/**
 * Created by hkthilina on 5/11/2016.
 */
public class ChallengeCountTask  extends AsyncTask<String, Void, Integer> {

    // public static int ID;
    // private GroupFragment context;
    private NewsFragment context;

    public ChallengeCountTask(NewsFragment context) {


        this.context = context;


    }


    @Override
    protected Integer doInBackground(String... params) {

        try {

            String userID = (String) params[0];
            String date = (String) params[1];

            //     Log.d("Steps", "steps date" + date);
            //   Log.d("Steps", "steps userID" + userID);

            String link = "http://sharethat.us/QUT/SearchChallengesCount.php";
            String data = URLEncoder.encode("userID", "UTF-8") + "=" + URLEncoder.encode(userID, "UTF-8");
            data += "&" + URLEncoder.encode("date", "UTF-8") + "=" + URLEncoder.encode(date, "UTF-8");


            URL url = new URL(link);

            URLConnection conn = url.openConnection();

            conn.setDoOutput(true);
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());

            wr.write(data);
            wr.flush();

            BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));

            StringBuilder sb = new StringBuilder();
            String line = null;


            // Read Server Response
            while ((line = reader.readLine()) != null) {
                // Log.d("Passed lines",sb.toString());

                //   Log.d("Passed lines",line);
                sb.append(line);
                // break;
            }
//            ID = Integer.parseInt(sb.toString());


            int steps = Integer.parseInt(sb.toString());
            //  Log.d("Steps", "steps are" + steps);
            return steps;


        } catch (Exception e) {
            Log.d("Exception", e.getMessage());
            return 0;
        }

    }

    @Override
    protected void onPostExecute(Integer integer) {
        super.onPostExecute(integer);

        try {
            context.setChallengeCountTask(integer);
        } catch (Exception e) {
            //Toast.makeText(context, "Ni internet", Toast.LENGTH_SHORT).show();
        }
    }
}
