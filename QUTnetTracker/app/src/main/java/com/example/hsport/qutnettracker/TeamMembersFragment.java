package com.example.hsport.qutnettracker;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;

import com.example.hsport.qutnettracker.Tasks.SearchGroupsTask;
import com.example.hsport.qutnettracker.Tasks.getMembersTask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class TeamMembersFragment extends Fragment implements AdapterView.OnItemSelectedListener {

    public MainActivity mainActivity = new MainActivity();
    public static Spinner spinner;
    public static List<String> GroupNames = new ArrayList<String>();
    public static List<String> friends = new ArrayList<String>();
    private SearchGroupsTask searchGroupsTask;
    private getMembersTask getMembers;

    public static View rootView;

    public TeamMembersFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
     //   Log.d("came heare", "settings");
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_team_members, container, false);


        searchGroupsTask = (SearchGroupsTask) new SearchGroupsTask(this).execute();


        return rootView;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        // An item was selected. You can retrieve the selected item using
        // parent.getItemAtPosition(pos)
     //   Log.d("Value", "Selected: ");
        String group = "0";
        try {
            Object item = parent.getItemAtPosition(position);
            group = item.toString();
            //  curID = spinner.getSelectedItem().toString();
     //       Log.d("Value", "Selected: " + group);
            //  mainActivity.setCurGeoID(curID);
            getMembers = (getMembersTask) new getMembersTask(this).execute(group);
        } catch (Exception E) {

        }
        // Spinner mySpinner=(Spinner) findViewById(R.id.your_spinner);


    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    public void setList(List<String> list) {
      //  Log.d("Setting", "Setting group");
        this.GroupNames = list;

        for (String item : GroupNames) {
           // Log.d("Group", "Group Name " + item);
        }


        spinner = (Spinner) rootView.findViewById(R.id.teams_spinner);
// Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, GroupNames);


        //  ArrayAdapter<String> adapter=new ArrayAdapter<String>(getActivity(),android.R.layout.simple_spinner_item,GroupNames);
//        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
//                R.array.planets_array, android.R.layout.simple_spinner_item);
// Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
// Apply the adapter to the spinner
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(this);

        //     ArrayAdapter<String> ArAd = new ArrayAdapter<String>(getActivity(), R.layout.rowlayout, R.id.txtItem, GroupNames);
//
//        //  ArrayAdapter<String> ArAd = new ArrayAdapter<String>(getActivity(),R.layout.rowlayout, R.id.txtItem,GroupNames);
//
//        ListView lv = (ListView) rootView.findViewById(R.id.m_list);
//        lv.setAdapter(ArAd);
//        //  setListAdapter(ArAd);
//        setRetainInstance(true);
//
//        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                ViewGroup rootView = (ViewGroup) view;
//                TextView groupName = (TextView) rootView.findViewById(R.id.txtItem);
//
//
//                Log.d("clicked", (String) groupName.getText());
//
//                //   Toast.makeText(getActivity(), textView.getText(), Toast.LENGTH_SHORT).show();
//                //  new SearchUserIDTask().execute(groupName.getText().toString());
//                searchUserIDTask = (SearchUserIDTask) new SearchUserIDTask(GroupFragment.this).execute(memID);
//                searchGroupIDtask = (SearchGroupIDtask) new SearchGroupIDtask(GroupFragment.this).execute(groupName.getText().toString().trim());
//                // searchUserIDTask=  new SearchUserIDTask(this).execute(memID);
//
//
//                new InsertToGroup().execute(ID, GID);
//
//            }
//        });

    }


    public void setFriends(List<String> list) {
     //   Log.d("Setting", "Setting group");
        this.friends = list;

        for (String item : friends) {
          //  Log.d("Group", "Group friends " + item);
        }

        int[] flags = new int[]{
                R.drawable.gold,
                R.drawable.silver,
                R.drawable.bronse,
                R.drawable.avatar,
                R.drawable.sad,

        };

        try {

            List<HashMap<String, String>> aList = new ArrayList<HashMap<String, String>>();

            int i = 0;
            for (String r : friends) {

                String[] parts = r.split(":");
                HashMap<String, String> hm = new HashMap<String, String>();

                hm.put("txt", parts[0]);
                hm.put("total", "Extra steps for the day: " + parts[1]);

//                hm.put("txt", r);
//                hm.put("total", "" );
                if (i < 3)
                    hm.put("extra", Integer.toString(flags[i]));
                else
                    hm.put("extra", Integer.toString(flags[3]));
                aList.add(hm);
                i++;

            }

            // Keys used in Hashmap
            String[] from = {"extra", "txt", "total"};

            // Ids of views in listview_layout
            int[] to = {R.id.flag, R.id.txt, R.id.cur};

            // Instantiating an adapter to store each items
            // R.layout.listview_layout defines the layout of each item
            SimpleAdapter adapter = new SimpleAdapter(getActivity(), aList, R.layout.listview_layout, from, to);

            // Getting a reference to listview of main.xml layout file
            ListView listView = (ListView) rootView.findViewById(R.id.G_list);

            // Setting the adapter to the listView
            listView.setAdapter(adapter);
        } catch (Exception e) {
            Log.e("Exception:", e.getMessage());
            List<HashMap<String, String>> aList = new ArrayList<HashMap<String, String>>();

            HashMap<String, String> hm = new HashMap<String, String>();
            hm.put("txt", "Leader board is empty.");
            //  hm.put("total", "Total steps : " + parts[1] + " Extra Steps: " + parts[2]);
            hm.put("total", "");
            hm.put("extra", Integer.toString(flags[4]));
            aList.add(hm);


            // Keys used in Hashmap
            String[] from = {"extra", "txt", "total"};

            // Ids of views in listview_layout
            int[] to = {R.id.flag, R.id.txt, R.id.cur};

            // Instantiating an adapter to store each items
            // R.layout.listview_layout defines the layout of each item
            SimpleAdapter adapter = new SimpleAdapter(getActivity(), aList, R.layout.listview_layout, from, to);

            // Getting a reference to listview of main.xml layout file
            ListView listView = (ListView) rootView.findViewById(R.id.G_list);

            // Setting the adapter to the listView
            listView.setAdapter(adapter);
        }

    }
}
