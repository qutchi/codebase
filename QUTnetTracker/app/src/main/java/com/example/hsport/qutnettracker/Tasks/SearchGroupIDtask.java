package com.example.hsport.qutnettracker.Tasks;

import android.os.AsyncTask;
import android.util.Log;

import com.example.hsport.qutnettracker.GroupFragment;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

/**
 * Created by hkthilina on 3/25/2016.
 */
public class SearchGroupIDtask extends AsyncTask<String,Void,Integer> {

    public static int ID;
    private GroupFragment context;

    public SearchGroupIDtask(GroupFragment context) {


        this.context = context;


    }
    @Override
    protected Integer doInBackground(String... params) {

        try{

            String Gr_ID = (String)params[0];
            //    String password = (String)params[1];

       //     Log.d("ID", "GID passing  is" + Gr_ID);
            String link="http://sharethat.us/QUT/SearchGroupID.php";
            String data  = URLEncoder.encode("Gr_ID", "UTF-8") + "=" + URLEncoder.encode(Gr_ID, "UTF-8");
            //    data += "&" + URLEncoder.encode("password", "UTF-8") + "=" + URLEncoder.encode(password, "UTF-8");


            URL url = new URL(link);

            URLConnection conn = url.openConnection();

            conn.setDoOutput(true);
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());

            wr.write( data );
            wr.flush();

            BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));

            StringBuilder sb = new StringBuilder();
            String line = null;




            // Read Server Response
            while((line = reader.readLine()) != null)
            {
                // Log.d("Passed lines",sb.toString());

                //   Log.d("Passed lines",line);
                sb.append(line);
                // break;
            }
            ID= Integer.parseInt(sb.toString());
           // Log.d("ID", "GID is" + ID);
            return ID;



        }
        catch(Exception e){
            Log.d("Exception",e.getMessage());
            return ID;
        }

    }

    @Override
    protected void onPostExecute(Integer integer) {

        super.onPostExecute(integer);
        context.setGroupID(integer);
    }
}
