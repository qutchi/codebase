package com.example.hsport.qutnettracker;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by hkthilina on 3/17/2016.
 */

public  class BlockLocation {
    private LatLng mLatLng;
    private String mId;
    private float mradius;
    private LatLng[] mnearby;

    BlockLocation(LatLng latlng, String id, float r, LatLng[] nearby) {
        setmLatLng(latlng);
        setmId(id);
        setMradius(r);
        setMnearby(nearby);
    }


    public LatLng getmLatLng() {
        return mLatLng;
    }

    public void setmLatLng(LatLng mLatLng) {
        this.mLatLng = mLatLng;
    }

    public String getmId() {
        return mId;
    }

    public void setmId(String mId) {
        this.mId = mId;
    }

    public float getMradius() {
        return mradius;
    }

    public void setMradius(float mradius) {
        this.mradius = mradius;
    }

    public LatLng[] getMnearby() {
        return mnearby;
    }

    public void setMnearby(LatLng[] mnearby) {
        this.mnearby = mnearby;
    }
}
