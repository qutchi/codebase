package com.example.hsport.qutnettracker.Tasks;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.ListView;

import com.example.hsport.qutnettracker.AsyncResponse;
import com.example.hsport.qutnettracker.OfferChallengeFragment;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by hkthilina on 3/15/2016.
 */
public class FriendsSearchTask extends AsyncTask<String,Void,List<String>> {

    private OfferChallengeFragment context;
    private ListView lv;
    public AsyncResponse delegate = null;

    public FriendsSearchTask(OfferChallengeFragment context) {


        this.context = context;


    }

    private List<String> Names = new ArrayList<String>();
    @Override
    protected List<String> doInBackground(String... params) {

        try{
            String username =(String)params[0];
            //    String password = (String)params[1];

            String link="http://sharethat.us/QUT/FriendsListSearch.php";
            String data  = URLEncoder.encode("username", "UTF-8") + "=" + URLEncoder.encode(username, "UTF-8");
            //    data += "&" + URLEncoder.encode("password", "UTF-8") + "=" + URLEncoder.encode(password, "UTF-8");


            URL url = new URL(link);

            URLConnection conn = url.openConnection();

            conn.setDoOutput(true);
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());

            wr.write( data );
            wr.flush();

            BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));

            StringBuilder sb = new StringBuilder();
            String line = null;



         //   Log.d("Friends List debug", "1");
           // int i=0;
            // Read Server Response
            while((line = reader.readLine()) != null)
            {
            //    Log.d("Friends List names", line);
                sb.append(line);

            }
            String String_Response = sb.toString(); // this is your web response

       Names = Arrays.asList(String_Response.split("\\s*,\\s*"));

          //  Log.d("Friends List Response", String_Response);
//
//            JSONArray jArray = new JSONArray(String_Response);
//            for (int i = 0; i < jArray.length(); i++)
//            {
//                JSONObject json_data = jArray.getJSONObject(i);
//                Names.add(json_data.getString("username"));
//            }

//            ArrayAdapter<String> ArAd = new ArrayAdapter<String>(context, R.layout.rowlayout, R.id.txtItem, Names);
//            setListAdapter(ArAd);
//            setRetainInstance(true);
            return Names;
        }
        catch(Exception e){
            Log.d("Friends List Exception", "2");
///          Log.d("Friends List Exception", e.getMessage().toString());
            return Names;
        }
    }

    @Override
    protected void onPostExecute(List<String> ss) {
        super.onPostExecute(ss);
        Log.d("Friends List debug", "2");
        context.setList(ss);
        //delegate.processFinish(ss);
    }
}
