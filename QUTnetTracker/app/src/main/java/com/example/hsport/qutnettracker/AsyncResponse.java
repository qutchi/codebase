package com.example.hsport.qutnettracker;

/**
 * Created by hkthilina on 3/13/2016.
 */
public interface AsyncResponse {
    void processFinish(String output);
}
