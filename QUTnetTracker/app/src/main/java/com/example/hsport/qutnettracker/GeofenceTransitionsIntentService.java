package com.example.hsport.qutnettracker;

import android.app.AlertDialog;
import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.WindowManager;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;
import com.google.android.gms.maps.model.LatLng;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by hkthilina on 3/9/2016.
 */
public class GeofenceTransitionsIntentService extends IntentService {
    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    MainActivity mainActivity = new MainActivity();
    public static String block;

    protected static final String TAG = "GeofenceTransitionsIS";

    public GeofenceTransitionsIntentService(String name) {
        super(TAG);
    }

    public GeofenceTransitionsIntentService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        try {
            GeofencingEvent geofencingEvent = GeofencingEvent.fromIntent(intent);
            if (geofencingEvent.hasError()) {
//            String errorMessage = GeofenceErrorMessages.getErrorString(this,
//                    geofencingEvent.getErrorCode());
                String errorMessage = "GeoFenceError";
                Log.e(TAG, errorMessage);
                return;
            }

            // Get the transition type.
            int geofenceTransition = geofencingEvent.getGeofenceTransition();


            Log.i(TAG, "Transition awa");


//        // Test that the reported transition was of interest.
//        if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER ||
//                geofenceTransition == Geofence.GEOFENCE_TRANSITION_EXIT ){

            if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_EXIT) {


                // Get the geofences that were triggered. A single event can trigger multiple geofences.
                List<Geofence> triggeringGeofences = geofencingEvent.getTriggeringGeofences();


                String myGeofence = "";
                //   List <Geofence> triggerList = getTriggeringGeofences(intent);

                String[] triggerIds = new String[triggeringGeofences.size()];

                for (int i = 0; i < triggerIds.length; i++) {
                    triggerIds[i] = triggeringGeofences.get(i).getRequestId();
               //     Log.d("MyGeo ids ", triggerIds[i]);
                    myGeofence = triggerIds[i];
                }


//            triggeringGeofences.get(1);

                // Get the transition details as a String.
                String geofenceTransitionDetails = getGeofenceTransitionDetails(
                        this,
                        geofenceTransition,
                        triggeringGeofences
                );

               // Log.d("MyGeo1 ", geofenceTransitionDetails);

                int challengeInProgress = mainActivity.getChallengeInProgress();
                //Uncomment this to get the geoId of geofence by splitting the string

//            for (String retval: geofenceTransitionDetails.split(":", 3)){
//                myGeofence=retval;
//
//            }

//        Log.d("MyGeo ", myGeofence);

                // String nearby = mainActivity.getNearbyToofferChallenge(myGeofence);
                String nearby = mainActivity.getNearByID();
//
//        Log.d("MyGeonearby ", nearby);

                if (challengeInProgress == 0)
                    sendNotification(nearby);
                Log.i(TAG, geofenceTransitionDetails);


//            if (myGeofence.equals(mainActivity.getCurrentGeoID())) {
//                Log.d("MyGeo ", myGeofence);
//
//               // String nearby = mainActivity.getNearbyToofferChallenge(myGeofence);
//                String nearby = mainActivity.getNearByID();
//
//                Log.d("MyGeonearby ", nearby);
//                sendNotification(nearby);
//                Log.i(TAG, geofenceTransitionDetails);
//            }

            } else if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER) {
                try {
                    Log.e(TAG, "geofence_transition_Entered");

                    BlockLocation destination = mainActivity.getDestinationBlock();
                    List<Geofence> triggeringGeofences = geofencingEvent.getTriggeringGeofences();

                    Log.e(TAG, "geofence_transition_Entered 1");


                    String myGeofence = "";
                    //   List <Geofence> triggerList = getTriggeringGeofences(intent);

                    String[] triggerIds = new String[triggeringGeofences.size()];

                    for (int i = 0; i < triggerIds.length; i++) {
                        triggerIds[i] = triggeringGeofences.get(i).getRequestId();
                        Log.d("MyGeo ids ", triggerIds[i]);
                        myGeofence = triggerIds[i];
                    }

//                if(myGeofence.equals("Entrance1")||myGeofence.equals("Entrance2")||myGeofence.equals("Entrance3")||myGeofence.equals("S Block"))
//                {
//                    mainActivity.StartSensors();
//                }



                    Log.e(TAG, "geofence_transition_Entered 2");

                    if (destination.getmId().equals(myGeofence)) {

                        if (mainActivity.getNotification() == true) {
                            mainActivity.updateChallegeCount();

                            mainActivity.setChallengeInProgress(0);
                            mainActivity.addCompletedChallenge(destination.getmId());

                            mainActivity.settNotification();

                            sendResult();
                            Intent intnt = new Intent("some_custom_id");
                            intnt.putExtra("message", "transiting");
                            LocalBroadcastManager.getInstance(this).sendBroadcast(intnt);
                            //  handler.sendEmptyMessage(0);

                            mainActivity.cancelSubscriptions();
                            mainActivity.setCurGeoID(myGeofence);
                            mainActivity.resetDestination();
                        }
                   }

                } catch (Exception ex) {
                    Log.e(TAG, "Exception" + ex.getMessage());
                }
            } else
                Log.e(TAG, "geofence_transition_invalid_type");

        } catch (Exception e) {

            Log.e("GeoFence","Gefence 3: "+e.getMessage());
        }
    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {

            mainActivity.sendConfirmation();
        }
    };

    //    private void offerChallengetoNearByFences(String myGeofence) {
//
//        LatLng[] nearby= blockLocation.getMnearby();
//
//    }
    private String getGeofenceTransitionDetails(
            Context context,
            int geofenceTransition,
            List<Geofence> triggeringGeofences) {

        try {

            String geofenceTransitionString = getTransitionString(geofenceTransition);

            // Get the Ids of each geofence that was triggered.
            ArrayList triggeringGeofencesIdsList = new ArrayList();
            for (Geofence geofence : triggeringGeofences) {
                triggeringGeofencesIdsList.add(geofence.getRequestId());
            }
            String triggeringGeofencesIdsString = TextUtils.join(", ", triggeringGeofencesIdsList);

            return geofenceTransitionString + ": " + triggeringGeofencesIdsString;
        } catch (Exception ex) {

            Log.e("Geofence", "GeoFenceTrans1" + ex.getMessage());
            return "error";
        }
    }

    private void sendNotification(String notificationDetails) {

        try {
            block=notificationDetails;

            // Create an explicit content Intent that starts the main Activity.
            Intent notificationIntent = new Intent(getApplicationContext(), MainActivity.class);
            notificationIntent.putExtra("mapFragment", "myMenuItem");
            // Construct a task stack.
            mainActivity.setFirstTime();
            TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
            // Add the main Activity to the task stack as the parent.
            stackBuilder.addParentStack(MainActivity.class);
            // Push the content Intent onto the stack.
            stackBuilder.addNextIntent(notificationIntent);
            // Get a PendingIntent containing the entire back stack.
            PendingIntent notificationPendingIntent =
                    stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
            // Get a notification builder that's compatible with platform versions >= 4
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
            // Define the notification settings.
            builder.setSmallIcon(R.mipmap.ic_launcher)
                    // In a real app, you may want to use a library like Volley
                    // to decode the Bitmap.
                    .setLargeIcon(BitmapFactory.decodeResource(getResources(),
                            R.mipmap.ic_launcher))
                    .setColor(Color.RED)
                    .setContentTitle("Challenge!! go to " + notificationDetails)
                    .setContentText(getString(R.string.geofence_transition_notification_text))
                    .setContentIntent(notificationPendingIntent);
            // Dismiss notification once the user touches it.
            builder.setAutoCancel(true);
            builder.setVibrate(new long[]{1000, 1000, 1000, 1000, 1000});
            builder.setDeleteIntent(getDeleteIntent());

            Bitmap bigBitmap;
            switch(notificationDetails) {
                case "A Block":
                   bigBitmap = BitmapFactory.decodeResource(this.getResources(), R.drawable.a);
                    break;
                case "B Block":
                    bigBitmap = BitmapFactory.decodeResource(this.getResources(), R.drawable.b);
                    break;
                case "C Block":
                    bigBitmap = BitmapFactory.decodeResource(this.getResources(), R.drawable.c);
                    break;
                case "D Block":
                    bigBitmap = BitmapFactory.decodeResource(this.getResources(), R.drawable.d);
                    break;
                case "E Block":
                    bigBitmap = BitmapFactory.decodeResource(this.getResources(), R.drawable.e);
                    break;
                case "F Block":
                    bigBitmap = BitmapFactory.decodeResource(this.getResources(), R.drawable.f);
                    break;
                case "G Block":
                    bigBitmap = BitmapFactory.decodeResource(this.getResources(), R.drawable.g);
                    break;
                case "H Block":
                    bigBitmap = BitmapFactory.decodeResource(this.getResources(), R.drawable.h);
                    break;
                case "M Block":
                    bigBitmap = BitmapFactory.decodeResource(this.getResources(), R.drawable.m);
                    break;
                case "N Block":
                    bigBitmap = BitmapFactory.decodeResource(this.getResources(), R.drawable.n);
                    break;
                case "O Block":
                    bigBitmap = BitmapFactory.decodeResource(this.getResources(), R.drawable.o);
                    break;
                case "P Block":
                    bigBitmap = BitmapFactory.decodeResource(this.getResources(), R.drawable.p);
                    break;
                case "Q Block":
                    bigBitmap = BitmapFactory.decodeResource(this.getResources(), R.drawable.q);
                    break;
                case "R Block":
                    bigBitmap = BitmapFactory.decodeResource(this.getResources(), R.drawable.r);
                    break;
                case "S Block":
                    bigBitmap = BitmapFactory.decodeResource(this.getResources(), R.drawable.s);
                    break;
                case "V Block":
                    bigBitmap = BitmapFactory.decodeResource(this.getResources(), R.drawable.v);
                    break;
                case "X Block":
                    bigBitmap = BitmapFactory.decodeResource(this.getResources(), R.drawable.x);
                    break;
                case "Y Block":
                    bigBitmap = BitmapFactory.decodeResource(this.getResources(), R.drawable.y);
                    break;
                case "Z Block":
                    bigBitmap = BitmapFactory.decodeResource(this.getResources(), R.drawable.z);
                    break;
                default:
                    bigBitmap = BitmapFactory.decodeResource(this.getResources(), R.drawable.x);
            }


//
//            Bitmap bigBitmap = BitmapFactory.decodeResource(this.getResources(), R.drawable.a);
            builder.setStyle(new android.support.v7.app.NotificationCompat.BigPictureStyle()
                    .bigPicture(bigBitmap));




            // Get an instance of the Notification manager
            NotificationManager mNotificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            // Issue the notification
            mNotificationManager.notify(0, builder.build());




        } catch (Exception ex) {
            Log.e("Geofence", "GeoFenceTrans1" + ex.getMessage());
        }
    }

    protected PendingIntent getDeleteIntent()
    {
        Intent intent = new Intent(getApplication(), NotificationBroadcastReceiver.class);
        intent.setAction("notification_cancelled");
       intent.putExtra("BlockValue", block);
        return PendingIntent.getBroadcast(getApplication(), 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
    }

    private void sendResult() {

        try {
            // Create an explicit content Intent that starts the main Activity.
            Intent notificationIntent = new Intent(getApplicationContext(), MainActivity.class);

            // Construct a task stack.
            TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
            // Add the main Activity to the task stack as the parent.
            stackBuilder.addParentStack(MainActivity.class);
            // Push the content Intent onto the stack.
            stackBuilder.addNextIntent(notificationIntent);
            // Get a PendingIntent containing the entire back stack.
            PendingIntent notificationPendingIntent =
                    stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
            // Get a notification builder that's compatible with platform versions >= 4
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
            // Define the notification settings.
            builder.setSmallIcon(R.drawable.trophy)
                    // In a real app, you may want to use a library like Volley
                    // to decode the Bitmap.
                    .setLargeIcon(BitmapFactory.decodeResource(getResources(),
                            R.drawable.trophy))
                    .setColor(Color.GREEN)
                    .setContentTitle("Challenge Complete")
                    .setContentText(getString(R.string.geofence_transition_notification_text))
                    .setContentIntent(notificationPendingIntent);
            // Dismiss notification once the user touches it.
            builder.setAutoCancel(true);
            builder.setVibrate(new long[]{1000, 1000, 1000, 1000, 1000});
            builder.setLights(Color.GREEN, 3000, 3000);

            Bitmap bigBitmap = BitmapFactory.decodeResource(this.getResources(), R.drawable.congratulations);
            builder.setStyle(new android.support.v7.app.NotificationCompat.BigPictureStyle()
                    .bigPicture(bigBitmap));

            // Get an instance of the Notification manager
            NotificationManager mNotificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            // Issue the notification
            mNotificationManager.notify(0, builder.build());
        } catch (Exception exx) {
            Log.e("Geofence", "GeoFenceTrans2" + exx.getMessage());
        }

    }

    private String getTransitionString(int transitionType) {
        switch (transitionType) {
            case Geofence.GEOFENCE_TRANSITION_ENTER:
                return getString(R.string.geofence_transition_entered);
            case Geofence.GEOFENCE_TRANSITION_EXIT:
                return getString(R.string.geofence_transition_exited);
            default:
                return "unknown_geofence_transition";// getString(R.string.unknown_geofence_transition);
        }
    }
}
