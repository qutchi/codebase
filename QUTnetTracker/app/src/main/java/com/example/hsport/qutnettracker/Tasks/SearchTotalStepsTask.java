package com.example.hsport.qutnettracker.Tasks;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.example.hsport.qutnettracker.MainActivity;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

/**
 * Created by hkthilina on 4/21/2016.
 */
public class SearchTotalStepsTask  extends AsyncTask<String, Void, String> {

    // public static int ID;
    // private GroupFragment context;
    private MainActivity context2=new MainActivity();

//    public SearchExtraStepsTask(GroupFragment context) {
//
//
//        this.context = context;
//
//
//    }



    @Override
    protected String doInBackground(String... params) {

        try {

            String userID = (String) params[0];
            String date = (String)params[1];

            //     Log.d("Steps", "steps date" + date);
            //   Log.d("Steps", "steps userID" + userID);

            String link = "http://sharethat.us/QUT/SearchTotalSteps.php";
            String data = URLEncoder.encode("userID", "UTF-8") + "=" + URLEncoder.encode(userID, "UTF-8");
            data += "&" + URLEncoder.encode("date", "UTF-8") + "=" + URLEncoder.encode(date, "UTF-8");


            URL url = new URL(link);

            URLConnection conn = url.openConnection();

            conn.setDoOutput(true);
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());

            wr.write(data);
            wr.flush();

            BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));

            StringBuilder sb = new StringBuilder();
            String line = null;


            // Read Server Response
            while ((line = reader.readLine()) != null) {
                // Log.d("Passed lines",sb.toString());

                //   Log.d("Passed lines",line);
                sb.append(line);
                // break;
            }
//            ID = Integer.parseInt(sb.toString());


            String steps=sb.toString();
            //  Log.d("Steps", "steps are" + steps);
            return steps.trim();


        } catch (Exception e) {
            Log.d("Exception", e.getMessage());
            return e.getMessage();
        }

    }

    @Override
    protected void onPostExecute(String steps) {

        super.onPostExecute(steps);

        //    Log.d("steps", "onPostExecute " + steps);

        try {
            context2.setTotSteps(steps);
        }catch (Exception e)
        {
            Toast.makeText(context2, "Ni internet", Toast.LENGTH_SHORT).show();
        }


    }

}
