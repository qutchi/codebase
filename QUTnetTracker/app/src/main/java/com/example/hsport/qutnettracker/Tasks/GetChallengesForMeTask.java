package com.example.hsport.qutnettracker.Tasks;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import com.example.hsport.qutnettracker.ChallengesForMeFragment;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by hkthilina on 3/30/2016.
 */
public class GetChallengesForMeTask extends AsyncTask<String, Void, List<String>> {


    private ChallengesForMeFragment mContext;
    //  private int NOTIFICATION_ID = 1;
    private Notification mNotification;
    private NotificationManager mNotificationManager;
    private String username;
    List<HashMap<String, String>> aList;

    public static String Challenger;
    public static String Block;
    public static String cDate;

    public GetChallengesForMeTask(ChallengesForMeFragment context) {

        mContext = context;
        // Name=name;
    }

    private List<String> Names = new ArrayList<String>();

    @Override
    protected List<String> doInBackground(String... params) {
        try {


         //   Log.d("calee", "Accepted");

            username = (String) params[0];
            //    String password = (String)params[1];

            Date dte = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");


            cDate= sdf.format(dte);
            String link = "http://sharethat.us/QUT/getmyChallenges.php";
            String data = URLEncoder.encode("username", "UTF-8") + "=" + URLEncoder.encode(username, "UTF-8");
              data += "&" + URLEncoder.encode("cDate", "UTF-8") + "=" + URLEncoder.encode(cDate, "UTF-8");


            URL url = new URL(link);

            URLConnection conn = url.openConnection();

            conn.setDoOutput(true);
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());

            wr.write(data);
            wr.flush();

            BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));

            StringBuilder sb = new StringBuilder();
            String line = null;


        //    Log.d("challenge List debug", "1");
            // int i=0;
            // Read Server Response
            while ((line = reader.readLine()) != null) {
                //Log.d("challenge List names", line);
                sb.append(line);

            }
            String String_Response = sb.toString(); // this is your web response

            Names = Arrays.asList(String_Response.split("\\s*,\\s*"));

           // Log.d("challenge List Response", String_Response);

            return Names;
        } catch (Exception e) {
            Log.d("challenge Exception", "2");
///          Log.d("Friends List Exception", e.getMessage().toString());
            return Names;
        }
    }

    @Override
    protected void onPostExecute(List<String> ss) {
        super.onPostExecute(ss);
        for (String s : ss) {
            Log.e("Strings", "chal " + s);
        }
        mContext.setmList(ss);


    }


}
