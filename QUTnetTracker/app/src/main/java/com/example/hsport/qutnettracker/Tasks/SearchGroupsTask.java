package com.example.hsport.qutnettracker.Tasks;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.ListView;

import com.example.hsport.qutnettracker.AsyncResponse;
import com.example.hsport.qutnettracker.GroupFragment;
import com.example.hsport.qutnettracker.TeamMembersFragment;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by hkthilina on 3/25/2016.
 */
public class SearchGroupsTask extends AsyncTask<String, Void, List<String>> {

    private GroupFragment context;
    private TeamMembersFragment context2;
    private ListView lv;
    public AsyncResponse delegate = null;

    public SearchGroupsTask(GroupFragment context) {


        this.context = context;


    }

    public SearchGroupsTask(TeamMembersFragment context) {


        Log.d("Setting context", "Context");
        this.context2 = context;


    }

    private List<String> GroupNames = new ArrayList<String>();

    @Override
    protected List<String> doInBackground(String... params) {
        try {
            //  String username =(String)params[0];
            //    String password = (String)params[1];

            String link = "http://sharethat.us/QUT/SearchGroups.php";
            //  String data  = URLEncoder.encode("username", "UTF-8") + "=" + URLEncoder.encode(username, "UTF-8");
            //    data += "&" + URLEncoder.encode("password", "UTF-8") + "=" + URLEncoder.encode(password, "UTF-8");


            URL url = new URL(link);

            URLConnection conn = url.openConnection();

            conn.setDoOutput(true);
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());

            // wr.write( );
            wr.flush();

            BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));

            StringBuilder sb = new StringBuilder();
            String line = null;


         //   Log.d("Group List debug", "1");
            // int i=0;
            // Read Server Response
            while ((line = reader.readLine()) != null) {
            //    Log.d("Group List names", line);
                sb.append(line);

            }
            String String_Response = sb.toString(); // this is your web response

            GroupNames = Arrays.asList(String_Response.split("\\s*,\\s*"));

          //  Log.d("Group List Response", String_Response);
//
//            JSONArray jArray = new JSONArray(String_Response);
//            for (int i = 0; i < jArray.length(); i++)
//            {
//                JSONObject json_data = jArray.getJSONObject(i);
//                Names.add(json_data.getString("username"));
//            }

//            ArrayAdapter<String> ArAd = new ArrayAdapter<String>(context, R.layout.rowlayout, R.id.txtItem, Names);
//            setListAdapter(ArAd);
//            setRetainInstance(true);
            return GroupNames;
        } catch (Exception e) {
         //   Log.d("Friends List Exception", "2");
///          Log.d("Friends List Exception", e.getMessage().toString());
            return GroupNames;
        }
    }

    @Override
    protected void onPostExecute(List<String> ss) {
        super.onPostExecute(ss);
     //   Log.d("Friends List debug", "Friends 2");

        if (context2 != null)
            context2.setList(ss);
        else if (context != null)
            context.setList(ss);
        //delegate.processFinish(ss);
    }
}
