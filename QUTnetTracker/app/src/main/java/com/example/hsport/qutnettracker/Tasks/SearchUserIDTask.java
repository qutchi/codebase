package com.example.hsport.qutnettracker.Tasks;

import android.os.AsyncTask;
import android.util.Log;

import com.example.hsport.qutnettracker.GroupFragment;
import com.example.hsport.qutnettracker.MainActivity;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Arrays;

/**
 * Created by hkthilina on 3/25/2016.
 */
public class SearchUserIDTask extends AsyncTask<String, Void, Integer> {

    public static int ID;
    private GroupFragment context;
    private MainActivity context2;

    public SearchUserIDTask(GroupFragment context) {


        this.context = context;


    }

    public SearchUserIDTask(MainActivity context) {


        this.context2 = context;


    }

    @Override
    protected Integer doInBackground(String... params) {

        try {

            String username = (String) params[0];
            //    String password = (String)params[1];

            String link = "http://sharethat.us/QUT/SearchUID.php";
            String data = URLEncoder.encode("username", "UTF-8") + "=" + URLEncoder.encode(username, "UTF-8");
            //    data += "&" + URLEncoder.encode("password", "UTF-8") + "=" + URLEncoder.encode(password, "UTF-8");


            URL url = new URL(link);

            URLConnection conn = url.openConnection();

            conn.setDoOutput(true);
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());

            wr.write(data);
            wr.flush();

            BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));

            StringBuilder sb = new StringBuilder();
            String line = null;


            // Read Server Response
            while ((line = reader.readLine()) != null) {
                // Log.d("Passed lines",sb.toString());

                //   Log.d("Passed lines",line);
                sb.append(line);
                // break;
            }
            ID = Integer.parseInt(sb.toString());
          //  Log.d("ID", "ID is" + ID);
            return ID;


        } catch (Exception e) {
            Log.d("Exception", e.getMessage());
            return ID;
        }

    }

    @Override
    protected void onPostExecute(Integer integer) {

        super.onPostExecute(integer);

        if (context2 != null)
            context2.setUserID(integer);
        else
            context.setUserID(integer);
    }
}
