package com.example.hsport.qutnettracker.Tasks;

import android.content.ContentValues;
import android.os.AsyncTask;
import android.util.Log;

import com.example.hsport.qutnettracker.JSONParser;
import com.example.hsport.qutnettracker.MainActivity;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hkthilina on 3/26/2016.
 */
public class AddStepDataTask extends AsyncTask<String,Void,String> {

    public JSONObject json ;
    private static final String TAG_SUCCESS = "code";
    private MainActivity context1;
    private String userID;
    private String Today;
    private String extraSteps;
    private String Totsteps;
    private static int ID;



    public void setUserID(int id) {
        ID = id;
    }



    @Override
    protected String doInBackground(String... params) {
        try{
            ContentValues contentValues = new ContentValues();
            userID = (String) params[0];
            Totsteps = (String) params[1];
            Today = (String) params[2];
            extraSteps=(String) params[3];

            String dates="";

         //   Log.d("Today", "date" + Today);

          //  Log.d("Async", "AsyncextraSteps" + extraSteps);
//
//            for (String retval: Today.split("-", 2)){
//                dates=retval;
//            }
//
//           //String dates= Today.split(" ", 2).toString();
//
//            Log.d("Today","date"+dates);

            List<NameValuePair> pair = new ArrayList<>();
            pair.add(new BasicNameValuePair("Member", userID));
            pair.add(new BasicNameValuePair("C_Date", Today));
            pair.add(new BasicNameValuePair("Tot_Steps", Totsteps));
            pair.add(new BasicNameValuePair("Extra_Steps", extraSteps));



            json = JSONParser.makeHttpRequest("http://sharethat.us/QUT/addSteps.php", "GET", pair);

         //   Log.d("Create Response", json.toString());

            return  "ok";

        }catch(Exception e){
            //Add an exception saying the name already exist
            return new String("Exception: " + e.getMessage());

        }
    }
}
