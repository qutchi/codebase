package com.example.hsport.qutnettracker.Tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.TextView;

import com.example.hsport.qutnettracker.AsyncResponse;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

/**
 * Created by hkthilina on 3/11/2016.
 */
public class LoginTask extends AsyncTask<String,Void,String> {
    private TextView statusField,roleField;
    private Context context;
    private int byGetOrPost = 0;



    public AsyncResponse delegate = null;


    //flag 0 means get and 1 means post.(By default it is get.)
    public LoginTask(Context context,TextView statusField,TextView roleField,int flag) {


        this.context = context;
        this.statusField = statusField;
        this.roleField = roleField;
        byGetOrPost = flag;

    }

    public LoginTask(Context context,int flag) {


        this.context = context;

        byGetOrPost = flag;
       // this.delegate = delegate;

    }


    @Override
    protected String doInBackground(String... params) {
        try{
            String username = (String)params[0];
        //    String password = (String)params[1];

            String link="http://sharethat.us/QUT/";
            String data  = URLEncoder.encode("username", "UTF-8") + "=" + URLEncoder.encode(username, "UTF-8");
       //    data += "&" + URLEncoder.encode("password", "UTF-8") + "=" + URLEncoder.encode(password, "UTF-8");


            URL url = new URL(link);

            URLConnection conn = url.openConnection();

            conn.setDoOutput(true);
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());

            wr.write( data );
            wr.flush();

            BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));

            StringBuilder sb = new StringBuilder();
            String line = null;




            // Read Server Response
            while((line = reader.readLine()) != null)
            {
               // Log.d("Passed lines",sb.toString());

             //   Log.d("Passed lines",line);
                sb.append(line);
               // break;
            }
            return sb.toString();



        }
        catch(Exception e){
            return new String("Exception: " + e.getMessage());
        }

       // return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        delegate.processFinish(s);
   //     this.statusField.setText("Login Successful");
      //  this.roleField.setText(s);


    }
}
