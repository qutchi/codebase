package com.example.hsport.qutnettracker.Tasks;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.ListView;

import com.example.hsport.qutnettracker.AsyncResponse;
import com.example.hsport.qutnettracker.NewsFragment;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

/**
 * Created by hkthilina on 4/20/2016.
 */
public class CompletedchallengesFeedtask extends AsyncTask<String,Void,List<String>> {

    private NewsFragment context;
    private ListView lv;
    public AsyncResponse delegate = null;
    private List<String> News = new ArrayList<String>();



    public CompletedchallengesFeedtask(NewsFragment context) {


        this.context = context;


    }
    @Override
    protected List<String> doInBackground(String... params) {
        try{
            String username =(String)params[0];
            //    String password = (String)params[1];

//            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
//            Calendar cal = Calendar.getInstance();
//
//            //Use this code to get yesterdays news
//            // cal.add(Calendar.DATE, -1);
//            cal.add(Calendar.DATE, -0);
//            String today=dateFormat.format(cal.getTime());
            String day = (String) params[1];

            Log.d("Yesterday","getdate"+day);

            String link="http://sharethat.us/QUT/getcompletedChllenges.php";
            String data  = URLEncoder.encode("C_date", "UTF-8") + "=" + URLEncoder.encode(day, "UTF-8");
            //    data += "&" + URLEncoder.encode("password", "UTF-8") + "=" + URLEncoder.encode(password, "UTF-8");


            URL url = new URL(link);

            URLConnection conn = url.openConnection();

            conn.setDoOutput(true);
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());

            wr.write( data );
            wr.flush();

            BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));

            StringBuilder sb = new StringBuilder();
            String line = null;



            //    Log.d("News debug", "1");
            // int i=0;
            // Read Server Response
            while((line = reader.readLine()) != null)
            {
                // Log.d("News names", line);
                sb.append(line);

            }
            String String_Response = sb.toString(); // this is your web response

            News = Arrays.asList(String_Response.split("\\s*,\\s*"));

            //   Log.d("News Response", String_Response);

            for(String n:News)
            {
                //Log.d("News Response", n);
            }
//
//            JSONArray jArray = new JSONArray(String_Response);
//            for (int i = 0; i < jArray.length(); i++)
//            {
//                JSONObject json_data = jArray.getJSONObject(i);
//                Names.add(json_data.getString("username"));
//            }

//            ArrayAdapter<String> ArAd = new ArrayAdapter<String>(context, R.layout.rowlayout, R.id.txtItem, Names);
//            setListAdapter(ArAd);
//            setRetainInstance(true);
            return News;
        }
        catch(Exception e){
            Log.d("News Exception", "2");
///          Log.d("Friends List Exception", e.getMessage().toString());
            return News;
        }
    }

    @Override
    protected void onPostExecute(List<String> strings) {
        super.onPostExecute(strings);
        context.setChallengesCompletedList(strings);
    }
}
