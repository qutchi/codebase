package com.example.hsport.qutnettracker.Tasks;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.ListView;

import com.example.hsport.qutnettracker.AsyncResponse;
import com.example.hsport.qutnettracker.TeamMembersFragment;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

/**
 * Created by hkthilina on 4/7/2016.
 */
public class getMembersTask  extends AsyncTask<String,Void,List<String>> {

    private TeamMembersFragment context;
    private ListView lv;
    public AsyncResponse delegate = null;

    public getMembersTask(TeamMembersFragment context) {


        this.context = context;


    }

    private List<String> Names = new ArrayList<String>();
    @Override
    protected List<String> doInBackground(String... params) {

        try{
            String group =(String)params[0];

            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
            Calendar cal = Calendar.getInstance();
            // cal.add(Calendar.DATE, -1);
            cal.add(Calendar.DATE, -0);
            String cdate=dateFormat.format(cal.getTime());
            //    String password = (String)params[1];

           // Log.d("Sending","Sending"+group);

            String link="http://sharethat.us/QUT/getFirendsinGroup.php";
            String data  = URLEncoder.encode("group", "UTF-8") + "=" + URLEncoder.encode(group.trim(), "UTF-8");
               data += "&" + URLEncoder.encode("cdate", "UTF-8") + "=" + URLEncoder.encode(cdate, "UTF-8");

           // Log.d("Sending","Sending data "+data);
            URL url = new URL(link);

            URLConnection conn = url.openConnection();

            conn.setDoOutput(true);
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());

            wr.write( data );
            wr.flush();

            BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));

            StringBuilder sb = new StringBuilder();
            String line = null;



          //  Log.d("Friends List debug", "1");
            // int i=0;
            // Read Server Response
            while((line = reader.readLine()) != null)
            {
               // Log.d("Friends List names", line);
                sb.append(line);

            }
            String String_Response = sb.toString(); // this is your web response

            Names = Arrays.asList(String_Response.split("\\s*,\\s*"));

       //     Log.d("Friends List Response", String_Response);
//
//            JSONArray jArray = new JSONArray(String_Response);
//            for (int i = 0; i < jArray.length(); i++)
//            {
//                JSONObject json_data = jArray.getJSONObject(i);
//                Names.add(json_data.getString("username"));
//            }

//            ArrayAdapter<String> ArAd = new ArrayAdapter<String>(context, R.layout.rowlayout, R.id.txtItem, Names);
//            setListAdapter(ArAd);
//            setRetainInstance(true);
            return Names;
        }
        catch(Exception e){
            Log.d("Friends List Exception", "2");
///          Log.d("Friends List Exception", e.getMessage().toString());
            return Names;
        }
    }

    @Override
    protected void onPostExecute(List<String> ss) {
        super.onPostExecute(ss);
    //    Log.d("Friends List debug", "2");
        context.setFriends(ss);
        //delegate.processFinish(ss);
    }
}
