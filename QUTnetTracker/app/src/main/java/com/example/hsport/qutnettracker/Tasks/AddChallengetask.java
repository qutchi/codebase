package com.example.hsport.qutnettracker.Tasks;

import android.content.ContentValues;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.example.hsport.qutnettracker.JSONParser;
import com.google.android.gms.plus.model.people.Person;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hkthilina on 3/15/2016.
 */
public class AddChallengetask  extends AsyncTask<String,Void,String> {


    public JSONObject json ;
    private static final String TAG_SUCCESS = "code";
    private Context context;
    private String challenger;
    private String chellengee;
    private String block;
    private String message;
    private String cdate;


    @Override
    protected String doInBackground(String... params) {

        try{
            ContentValues contentValues = new ContentValues();
            challenger = (String) params[0];;
            chellengee = (String) params[1];;
            block = (String) params[2];;
            message = (String) params[3];;
            cdate = (String) params[4];;



            List<NameValuePair> pair = new ArrayList<>();
            pair.add(new BasicNameValuePair("Challenger", challenger));
            pair.add(new BasicNameValuePair("Challengee", chellengee));
            pair.add(new BasicNameValuePair("Block", block));
            pair.add(new BasicNameValuePair("Message", message));
            pair.add(new BasicNameValuePair("cDate", cdate));



            json = JSONParser.makeHttpRequest("http://sharethat.us/QUT/addChallenge.php", "GET", pair);

          //  Log.d("Create Response", json.toString());

            return  "ok";

        }catch(Exception e){
            return new String("Exception: " + e.getMessage());

        }


    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        Log.d("Passed result", s);
    }
}
