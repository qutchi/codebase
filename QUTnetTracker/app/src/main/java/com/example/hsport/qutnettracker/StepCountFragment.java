package com.example.hsport.qutnettracker;


import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.fitness.FitnessStatusCodes;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.data.Subscription;
import com.google.android.gms.fitness.result.ListSubscriptionsResult;


/**
 * A simple {@link Fragment} subclass.
 */
public class StepCountFragment extends Fragment implements View.OnClickListener {


    public MainActivity mainActivity;
    private Button mButtonViewWeek;
    private Button mButtonViewToday;
    private Button mButtonAddSteps;
    private Button mButtonUpdateSteps;
    private Button mButtonDeleteSteps;
    private Button mCancelSubscriptionsBtn;
    private Button mShowSubscriptionsBtn;

    private ResultCallback<Status> mSubscribeResultCallback;
    private ResultCallback<Status> mCancelSubscriptionResultCallback;
    private ResultCallback<ListSubscriptionsResult> mListSubscriptionsResultCallback;


    public StepCountFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mainActivity= (MainActivity) activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View rootView = inflater.inflate(R.layout.fragment_step_count, container, false);
        mButtonViewWeek = (Button) rootView.findViewById(R.id.btn_view_week);
        mButtonViewToday = (Button) rootView.findViewById(R.id.btn_view_today);
//       mButtonAddSteps = (Button) rootView.findViewById(R.id.btn_add_steps);
   //    mButtonUpdateSteps = (Button) rootView.findViewById(R.id.btn_update_steps);
     //   mButtonDeleteSteps = (Button) rootView.findViewById(R.id.btn_delete_steps);
      mCancelSubscriptionsBtn = (Button) rootView.findViewById(R.id.btn_cancel_subscriptions);
//        mShowSubscriptionsBtn = (Button) rootView.findViewById(R.id.btn_show_subscriptions);

        mCancelSubscriptionsBtn.setOnClickListener(this);
    //    mShowSubscriptionsBtn.setOnClickListener(this);

        mButtonViewWeek.setOnClickListener(this);
        mButtonViewToday.setOnClickListener(this);
//       mButtonAddSteps.setOnClickListener(this);
  //     mButtonUpdateSteps.setOnClickListener(this);
    //    mButtonDeleteSteps.setOnClickListener(this);




        return rootView;
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
//            case R.id.btn_show_subscriptions: {
//                mainActivity.showSubscriptions();
//                break;
//            }
            case R.id.btn_cancel_subscriptions: {
                mainActivity.cancelSubscriptions();
                break;
            }
            case R.id.btn_view_week: {
                new ViewWeekStepCountTask().execute();
                break;
            }
            case R.id.btn_view_today: {
                new ViewTodaysStepCountTask().execute();
                break;
            }
//            case R.id.btn_add_steps: {
//                new AddStepsToGoogleFitTask().execute();
//                break;
//            }
//            case R.id.btn_update_steps: {
//                new UpdateStepsOnGoogleFitTask().execute();
//                break;
//            }
//            case R.id.btn_delete_steps: {
//                new DeleteYesterdaysStepsTask().execute();
//                break;
//            }
        }

    }

    private class ViewWeekStepCountTask extends AsyncTask<Void, Void, Void> {
        protected Void doInBackground(Void... params) {
            mainActivity.displayLastWeeksData();
            return null;
        }
    }

    private class ViewTodaysStepCountTask extends AsyncTask<Void, Void, Void> {
        protected Void doInBackground(Void... params) {
            mainActivity.displayStepDataForToday();
            return null;
        }
    }

    private class AddStepsToGoogleFitTask extends AsyncTask<Void, Void, Void> {
        protected Void doInBackground(Void... params) {
           //mainActivity.addStepDataToGoogleFit();
          //  mainActivity.displayLastWeeksData();
            return null;
        }
    }

    private class UpdateStepsOnGoogleFitTask extends AsyncTask<Void, Void, Void> {
        protected Void doInBackground(Void... params) {
            mainActivity.updateStepDataOnGoogleFit();
            mainActivity.displayLastWeeksData();
            return null;
        }
    }

    private class DeleteYesterdaysStepsTask extends AsyncTask<Void, Void, Void> {
        protected Void doInBackground(Void... params) {
            mainActivity.deleteStepDataOnGoogleFit();
            mainActivity.displayLastWeeksData();
            return null;
        }
    }


}
