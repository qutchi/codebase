package com.example.hsport.qutnettracker.Tasks;

import android.content.ContentValues;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.example.hsport.qutnettracker.JSONParser;
import com.example.hsport.qutnettracker.MainActivity;
import com.google.android.gms.plus.model.people.Person;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hkthilina on 3/30/2016.
 */
public class updateStatusTask extends AsyncTask<String,Void,String> {

    public JSONObject json ;
    private static final String TAG_SUCCESS = "code";
    private Context context;
    private Person person;
    MainActivity mainActivity = new MainActivity();

    public updateStatusTask() {
        json=new JSONObject();

        // this.delegate = delegate;
    }
    @Override
    protected String doInBackground(String... params) {
        try {
            ContentValues contentValues = new ContentValues();


            String status = (String) params[1];
            String C_ID = (String) params[0];
            List<NameValuePair> pair = new ArrayList<>();
            pair.add(new BasicNameValuePair("status", status));
            pair.add(new BasicNameValuePair("C_ID", C_ID));

            Log.d("Value", "Selected status: " + status);
            Log.d("Value", "Selected C_ID: " + C_ID);


            json = JSONParser.makeHttpRequest("http://sharethat.us/QUT/updateStatus.php", "GET", pair);

            Log.d("Create Response", json.toString());
             Log.d("Create Response2", json.getJSONArray("data").toString());


//        try {

//           int success = json.getInt(TAG_SUCCESS);
//           //getstatus = success;
//            if (success == 1) {
//
//                Log.d("success!", json.toString());
//
//            }
//
//            else if (success==0){
//
//
//                return json.getString(TAG_SUCCESS);
//
//            }


//        } catch (JSONException e) {
//            return new String("Exception: " + e.getMessage());
//        }
            return "ok";

        }  catch (Exception e) {
            // Toast.makeText(context,"Check your internet connectivity",Toast.LENGTH_SHORT);
            return "Exception";
        }
    }
}
