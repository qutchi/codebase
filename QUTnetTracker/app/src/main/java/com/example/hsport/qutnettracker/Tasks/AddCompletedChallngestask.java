package com.example.hsport.qutnettracker.Tasks;

import android.content.ContentValues;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.example.hsport.qutnettracker.JSONParser;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hkthilina on 4/20/2016.
 */
public class AddCompletedChallngestask  extends AsyncTask<String,Void,String> {

    public JSONObject json ;
    private static final String TAG_SUCCESS = "code";
    private Context context;
    private String member;
    private String block;
    private String CompDate;

    @Override
    protected String doInBackground(String... params) {
        try{
            ContentValues contentValues = new ContentValues();
            member = (String) params[0];

            block = (String) params[1];
            CompDate = (String) params[2];

            Log.e("EEE", "calling completed 2");
            Log.e("EEE", "calling completed 2"+ member);
            Log.e("EEE", "calling completed 2"+ block);
            Log.e("EEE", "calling completed 2"+ CompDate);

            List<NameValuePair> pair = new ArrayList<>();
            pair.add(new BasicNameValuePair("Member", member));
            pair.add(new BasicNameValuePair("Block", block));
            pair.add(new BasicNameValuePair("CompDate", CompDate));


            Log.e("EEE", "calling completed 3");

            json = JSONParser.makeHttpRequest("http://sharethat.us/QUT/addCompletedChallenges.php", "GET", pair);

            //  Log.d("Create Response", json.toString());

            return  "ok";

        }catch(Exception e){
            Log.e("EEE", "calling completed 4");
            return new String("Exception: " + e.getMessage());

        }


    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);

        Log.d("Passed result", s);
    }
}
