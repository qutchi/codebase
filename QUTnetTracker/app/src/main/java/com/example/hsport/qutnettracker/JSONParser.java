package com.example.hsport.qutnettracker;

import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.List;

/**
 * Created by hkthilina on 3/13/2016.
 */
public class JSONParser {

    static InputStream is = null;
    static JSONObject jObj = null;
    static String json = "";
    public static String Result=null;

    public JSONParser()
    {

    }

    public static JSONObject makeHttpRequest(String url,String method, List<NameValuePair> pair) {

        try {

            // check for request method
            if(method.equals("POST")){
                // request method is POST
                // defaultHttpClient
                DefaultHttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(url);
                httpPost.setEntity(new UrlEncodedFormEntity(pair));

                HttpResponse httpResponse = httpClient.execute(httpPost);
                HttpEntity httpEntity = httpResponse.getEntity();
                is = httpEntity.getContent();

            }else if(method.equals("GET")){

              //  Log.d("came","1");
                // request method is GET
                DefaultHttpClient httpClient = new DefaultHttpClient();
            //    Log.d("came","2");
                String paramString = URLEncodedUtils.format(pair, "utf-8");
            //    Log.d("came","3");
                url += "?" + paramString;
            //    Log.d("came","4");
                HttpGet httpGet = new HttpGet(url);
             //   Log.d("came","5");
                HttpResponse httpResponse = httpClient.execute(httpGet);
             //   Log.d("came","6");
                HttpEntity httpEntity = httpResponse.getEntity();
            //    Log.d("came","7");
                is = httpEntity.getContent();
            //    Log.d("came","8");
            }

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, HTTP.UTF_8), 8);
          //  Log.d("came","9");
            StringBuilder sb = new StringBuilder();
           // Log.d("came","10");
            String line = null;
            while ((line = reader.readLine()) != null) {
            //    Log.d("came lines",line);
                sb.append(line + "\n");
            }
          //  Log.d("came","12");
            is.close();
          //  Log.d("came", "13");
            json = sb.toString();
          //  Log.d("came","14");
            Result = json;
          //  Log.d("came",Result);
        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }

        // try parse the string to a JSON object
        try {
            jObj = new JSONObject(json);
          //  Log.d("came4",jObj.toString());
        } catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }

        return jObj;
    }


}
