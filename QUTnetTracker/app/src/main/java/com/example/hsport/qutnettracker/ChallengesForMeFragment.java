package com.example.hsport.qutnettracker;


import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.hsport.qutnettracker.Tasks.GetChallengesForMeTask;
import com.example.hsport.qutnettracker.Tasks.updateStatusTask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class ChallengesForMeFragment extends Fragment implements ListView.OnItemClickListener {


    private TextView txtView;
    public MainActivity mainActivity = new MainActivity();
    public Spinner spinner;
    public static GetChallengesForMeTask getChallengesForMeTask;
    ListView lv;
    public static List<String> list = new ArrayList<String>();
    static List<String> Names = new ArrayList<String>();
    public static View rootView;
    public static String ID;
    public ChallengesForMeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_challenges_for_me, container, false);

        txtView = (TextView) rootView.findViewById(R.id.txtMesseage);

        int[] flags = new int[]{
                R.drawable.man,
                R.drawable.man,

        };

        try {

            String id = mainActivity.getUserName();

            getChallengesForMeTask = (GetChallengesForMeTask) new GetChallengesForMeTask(this).execute((id));
        }
        catch (Exception s)
        {
            Log.e("Challenges4me",s.getMessage());

        }
//
//        //   ListView lv = (ListView) rootView.findViewById(R.id.list_news);
//
//
//        List<HashMap<String, String>> aList = new ArrayList<HashMap<String, String>>();
//        Log.e("came", "came:" + 1);
//        for (String r : list) {
//            Log.e("came", "came:" + 2);
//            Log.d("Returened", "returned " + r);
//            String[] parts = r.split(":");
//            HashMap<String, String> hm = new HashMap<String, String>();
//            hm.put("Challenger", parts[0]);
//            hm.put("Block", "Go to : " + parts[1] + " Message: " + parts[2]);
//            hm.put("extra", Integer.toString(flags[0]));
//            hm.put("ID", parts[3]);
//            aList.add(hm);
//
//
//        }
//        Log.e("came", "came:" + 3);
//        // Keys used in Hashmap
//        String[] from = {"extra", "Challenger", "Block"};
//
//        // Ids of views in listview_layout
//        int[] to = {R.id.flag, R.id.txt, R.id.cur};
//
//        // Instantiating an adapter to store each items
//        // R.layout.listview_layout defines the layout of each item
//        SimpleAdapter adapter = new SimpleAdapter(getActivity(), aList, R.layout.listview_layout, from, to);
//
//        // Getting a reference to listview of main.xml layout file
//        ListView listView = (ListView) rootView.findViewById(R.id.newslistview);
//
//        // Setting the adapter to the listView
//        listView.setAdapter(adapter);
//
//        listView.setOnItemClickListener(this);

        return rootView;
    }

    public void setmList(List<String> list) {
        int[] flags = new int[]{
                R.drawable.man1,
                R.drawable.sad,

        };
        this.list = list;
     try {


         List<HashMap<String, String>> aList = new ArrayList<HashMap<String, String>>();
       //  Log.e("came", "came:" + 1);
         for (String r : list) {
         //    Log.e("came", "came:" + 2);
           //  Log.d("Returened", "returned " + r);
             String[] parts = r.split("::");
             HashMap<String, String> hm = new HashMap<String, String>();
             hm.put("Challenger", parts[0]+" Message: " + parts[2] );
             hm.put("Block"," Go to : " + parts[1] + " Offered at: "+parts[4]);
             hm.put("extra", Integer.toString(flags[0]));
             hm.put("ID", parts[3]);
             aList.add(hm);


         }
         //Log.e("came", "came:" + 3);
         // Keys used in Hashmap
         String[] from = {"extra", "Challenger", "Block"};

         // Ids of views in listview_layout
         int[] to = {R.id.flag, R.id.txt, R.id.cur};

         // Instantiating an adapter to store each items
         // R.layout.listview_layout defines the layout of each item
         SimpleAdapter adapter = new SimpleAdapter(getActivity(), aList, R.layout.listview_layout, from, to);

         // Getting a reference to listview of main.xml layout file
         ListView listView = (ListView) rootView.findViewById(R.id.newslistview);

         // Setting the adapter to the listView
         listView.setAdapter(adapter);

         listView.setOnItemClickListener(this);
         txtView.setVisibility(View.GONE);
     }
     catch (Exception x){}

    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

     //   Log.d("Value", "Selected: ");
        //final String challengee="0";

        Object item = parent.getItemAtPosition(position);
        final String challengee = item.toString();


        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Do you want to accept this challenge?")
                .setCancelable(false)
                .setPositiveButton("Accept", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        //System.exit(0);
                        try {


                           // {extra=2130837659, Block= Go to : S BlockOffered at: 2016-04-20 12:00:46, ID=44, Challenger=Dhaval Vyas Message: hi}


                            //  mainActivity.startChallenge();
                         //   Log.d("called main", "called main");
                            String[] parts = challengee.split(",");
                            Log.d("Value", "Selected String: " + challengee.trim());

                            String block = parts[1].trim().toString();
                            String[] parts3 = block.split(":");

                            String block2 = parts3[1].trim().toString();
                            String[] parts4 = block2.split(" ");


                       ID = parts[2].trim().toString();
                            String[] parts2 = ID.split("=");


                            ID = parts2[1];

                            Log.d("Value", "Selected ID: " + ID.trim());

                            //uncomment the next line if commented
                        new updateStatusTask().execute(ID, "Accepted");





                            String blockID = parts4[0] + " " + parts4[1];
                           Log.d("Value", "Selected blockID: " + blockID.trim());
                            mainActivity.setNearByBlock(blockID.trim());

                           // mainActivity.displayMap();

                            Intent notificationIntent = new Intent(getActivity(), MainActivity.class);
                            notificationIntent.putExtra("mapFragment", "myOfferedChallenge");
                            // Construct a task stack.
                            TaskStackBuilder stackBuilder = TaskStackBuilder.create(getActivity());
                            // Add the main Activity to the task stack as the parent.
                            stackBuilder.addParentStack(MainActivity.class);
                            // Push the content Intent onto the stack.
                            stackBuilder.addNextIntent(notificationIntent);

                            startActivity(notificationIntent);
                           // mainActivity.StartSensors();

                            // 20 minutes in miliseconds coundowntimer
                            new CountDownTimer(1200000, 1000) {
                          //  new CountDownTimer(10000, 1000) {

                                public void onTick(long millisUntilFinished) {
                                    Log.e("Timing out","Time is ticking");
                                }

                                public void onFinish() {
                                    //finish() // finish ActivityB
                                    Log.e("Timing out","Timingout");
                                    new updateStatusTask().execute(ID, "TimedOut");
                                    mainActivity.setChallengeInProgress(0);
                                    Intent notificationIntent = new Intent(getActivity(), MainActivity.class);
                                    notificationIntent.putExtra("mapFragment", "myOfferedChallengeTimedout");
                                    // Construct a task stack.
                                    TaskStackBuilder stackBuilder = TaskStackBuilder.create(getActivity());
                                    // Add the main Activity to the task stack as the parent.
                                    stackBuilder.addParentStack(MainActivity.class);
                                    // Push the content Intent onto the stack.
                                    stackBuilder.addNextIntent(notificationIntent);

                                    startActivity(notificationIntent);
                                }
                            }.start();


                        } catch (Exception E) {
                            Log.e("Exception", E.getMessage());

                        }

                    }
                })
                .setNegativeButton("Reject", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        //dialog.cancel();
                        String[] parts = challengee.split(",");
                        Log.d("Value", "Selected String: " + challengee.trim());

                        String block = parts[1].trim().toString();
                        String[] parts3 = block.split(":");

                        String block2 = parts3[1].trim().toString();
                        String[] parts4 = block2.split(" ");


                        ID = parts[2].trim().toString();
                        String[] parts2 = ID.split("=");


                        ID = parts2[1];

                        Log.d("Value", "Selected ID: " + ID.trim());

                        new updateStatusTask().execute(ID, "Rejected");


                        ChallengesForMeFragment challengesForMeFragment = new ChallengesForMeFragment();
                        FragmentManager fragmentManager = getFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        //  FragmentManager fragmentManager = getFragmentManager();
                        // android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                        fragmentTransaction.replace(R.id.fragment_container, challengesForMeFragment);
                        fragmentTransaction.addToBackStack(null);
                        fragmentTransaction.commit();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }


//    private void sendNotification(String notificationDetails) {
//        // Create an explicit content Intent that starts the main Activity.
//        Intent notificationIntent = new Intent(getApplicationContext(), MainActivity.class);
//        notificationIntent.putExtra("mapFragment", "myMenuItem");
//        // Construct a task stack.
//        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
//        // Add the main Activity to the task stack as the parent.
//        stackBuilder.addParentStack(MainActivity.class);
//        // Push the content Intent onto the stack.
//        stackBuilder.addNextIntent(notificationIntent);
//        // Get a PendingIntent containing the entire back stack.
//        PendingIntent notificationPendingIntent =
//                stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
//        // Get a notification builder that's compatible with platform versions >= 4
//        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
//        // Define the notification settings.
//        builder.setSmallIcon(R.mipmap.ic_launcher)
//                // In a real app, you may want to use a library like Volley
//                // to decode the Bitmap.
//                .setLargeIcon(BitmapFactory.decodeResource(getResources(),
//                        R.mipmap.ic_launcher))
//                .setColor(Color.RED)
//                .setContentTitle("Challenge!! go to " + notificationDetails)
//                .setContentText(getString(R.string.geofence_transition_notification_text))
//                .setContentIntent(notificationPendingIntent);
//        // Dismiss notification once the user touches it.
//        builder.setAutoCancel(true);
//        builder.setVibrate(new long[]{1000, 1000, 1000, 1000, 1000});
//        // Get an instance of the Notification manager
//        NotificationManager mNotificationManager =
//                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//
//        // Issue the notification
//        mNotificationManager.notify(0, builder.build());
//    }
}
