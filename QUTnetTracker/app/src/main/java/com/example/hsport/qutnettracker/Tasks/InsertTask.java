package com.example.hsport.qutnettracker.Tasks;

import android.content.ContentValues;
import android.content.Context;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.widget.Toast;

import com.example.hsport.qutnettracker.JSONParser;
import com.example.hsport.qutnettracker.MainActivity;
import com.google.android.gms.plus.model.people.Person;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by hkthilina on 3/13/2016.
 */
public class InsertTask extends AsyncTask<String,Void,String> {

    public JSONObject json ;
    private static final String TAG_SUCCESS = "code";
    private Context context;
    private Person person;

    MainActivity mainActivity = new MainActivity();
    public InsertTask(Context c, Person p) {
        json=new JSONObject();
        this.context = c;
        person = p;
        // this.delegate = delegate;
    }
    @Override
    protected String doInBackground(String... params) {

       try {
           ContentValues contentValues = new ContentValues();
           String username = (String) params[0];
           String urlToPhoto = (String) params[1];
           // String urlToPhoto = "URL";
           String role = (String) params[2];
//        contentValues.put("Username", username);
//        contentValues.put("URL", urlToPhoto);
//        contentValues.put("Role", role);


           List<NameValuePair> pair = new ArrayList<>();
           pair.add(new BasicNameValuePair("Username", username));
           pair.add(new BasicNameValuePair("URL", urlToPhoto));
           pair.add(new BasicNameValuePair("Role", role));
           //   pair.add(new BasicNameValuePair("status",String,psswd);


           json = JSONParser.makeHttpRequest("http://sharethat.us/QUT/addUser.php", "GET", pair);

    //       Log.d("Create Response", json.toString());
           // Log.d("Create Response2", json.getJSONArray("data").toString());


//        try {

//           int success = json.getInt(TAG_SUCCESS);
//           //getstatus = success;
//            if (success == 1) {
//
//                Log.d("success!", json.toString());
//
//            }
//
//            else if (success==0){
//
//
//                return json.getString(TAG_SUCCESS);
//
//            }

           return "ok";
//        } catch (JSONException e) {
//            return new String("Exception: " + e.getMessage());
//        }

       }  catch (Exception e) {
          // Toast.makeText(context,"Check your internet connectivity",Toast.LENGTH_SHORT);
            return new String("Exception: " + e.getMessage());
        }

//        try {
//            URL url = new URL("http://specialoffers.lk/QUT/addUser.php");
//            HttpURLConnection conn = (HttpURLConnection)url.openConnection();
//            conn.setConnectTimeout(10000);
//            conn.setReadTimeout(10000);
//            conn.setRequestMethod("POST");
//            conn.setDoInput(true);
//            conn.setDoOutput(true);
//            OutputStream out = conn.getOutputStream();
//            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out,"UTF-8"));
//            writer.write(String.valueOf(params));
//            writer.flush();
//            writer.close();
//            out.close();
//            return "ok";
//        } catch (java.io.IOException e) {
//            return new String("Exception: " + e.getMessage());
//        }





    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);

          Log.d("Passed result", s);
    }
}